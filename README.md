|
--- | ---
All branches | [![Build status](https://ci.appveyor.com/api/projects/status/l8tx4d360775rc89?svg=true)](https://ci.appveyor.com/project/VladislavAntonyuk/hospital-management-system) 
Master | [![Build status](https://ci.appveyor.com/api/projects/status/l8tx4d360775rc89/branch/master?svg=true)](https://ci.appveyor.com/project/VladislavAntonyuk/hospital-management-system/branch/master)

# HospitalManagementSystem

C# + MS SQL SERVER. 

The work at hospital is organized with the program. 4 groups: Admin, Doctor, Nurse, Patient. 
Program gives to patients list of recommended medicines, death percent of his deseases etc.
Program helps to doctors looks for patients, finds information which is useful and nessesary for doctor.
Admin can create and save reports, add,delete and update information about patients, doctors, nurses.

It can be used at any hospital.
