﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SplashScreen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        DispatcherTimer OpacityTmr = new DispatcherTimer();
        DispatcherTimer WaitTmr = new DispatcherTimer();


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Uri resourceUri = new Uri("Resources/Background.png", UriKind.Relative);
            //StreamResourceInfo streamInfo = Application.GetResourceStream(resourceUri);

            //BitmapFrame temp = BitmapFrame.Create(streamInfo.Stream);
            //var brush = new ImageBrush();
            //brush.ImageSource = temp;

            //Background = brush;

            OpacityTmr.Tick += new EventHandler(OpacityTmr_Tick);
            OpacityTmr.Interval = new TimeSpan(0, 0, 0, 0, 35);
            OpacityTmr.Start();

            WaitTmr.Tick += new EventHandler(WaitTmr_Tick);
            WaitTmr.Interval = new TimeSpan(0, 0, 0,1);
            

        }
        private void WaitTmr_Tick(object sender, EventArgs e)
        {
            WaitTmr.Stop();
            try
            {
                Process.Start("HospitalManagementSystem.exe");
                
            }
            catch (Exception)
            {
                MessageBox.Show("HospitalManagementSystem.exe не знайдено. Перевірте наявність програми");
            }
            finally
            {
                Environment.Exit(0);
            }
        }
        private void OpacityTmr_Tick(object sender, EventArgs e)
        {
            if (this.Opacity >= 1)
            {
                OpacityTmr.Stop();
                WaitTmr.Start();
            }
            else
            {
                this.Opacity += 0.01;
            }
        }
    }
}
