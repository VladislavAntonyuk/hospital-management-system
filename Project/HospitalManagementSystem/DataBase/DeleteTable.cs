﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.DataBase
{
    public partial class DeleteTable : Form
    {
        public DeleteTable()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);
        private void renameBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (tableListCmb.Items.Contains(tableListCmb.Text))
                {
                    string sql = string.Format(@"DROP TABLE {0}", tableListCmb.SelectedItem);
                    SqlCommand renameSql = new SqlCommand(sql, conn);
                    renameSql.ExecuteNonQuery();
                    GlobalVariables.Journal(conn, string.Format("table {0} deleted successfully", tableListCmb.SelectedItem), "DeleteTable");
                    tableListCmb.Items.Remove(tableListCmb.Text);
                    tableListCmb.SelectedIndex = 0;
                    MessageBox.Show("Видалено");
                }
                else MessageBox.Show("Такої таблиці не існує");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Таблиця не видалена" + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void DeleteTable_Load(object sender, EventArgs e)
        {
            conn.Open();
            DataTable dt = conn.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tableListCmb.Items.Add(tablename);
            }
            tableListCmb.Items.Remove("sysdiagrams");
            tableListCmb.SelectedIndex = 0;
        }
    }
}
