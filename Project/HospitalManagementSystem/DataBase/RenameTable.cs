﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.DataBase
{
    public partial class RenameTable : Form
    {
        public RenameTable()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);
        private void renameBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (tableListCmb.Items.Contains(tableListCmb.Text) && newTableTxt.TextLength > 0)
                {
                    SqlCommand renameSql = new SqlCommand(@"EXEC sp_rename @oldName, @newName", conn);
                    renameSql.Parameters.AddWithValue("@oldName", tableListCmb.SelectedItem);
                    renameSql.Parameters.AddWithValue("@newName", newTableTxt.Text);
                    renameSql.ExecuteNonQuery();
                    GlobalVariables.Journal(conn, string.Format("table {0} renamed successfully into {1}", tableListCmb.SelectedItem, newTableTxt.Text), "RenameTable");
                    tableListCmb.Items[tableListCmb.SelectedIndex] = newTableTxt.Text;
                    tableListCmb.SelectedIndex = 0;
                    MessageBox.Show("Перейменовано");
                }
                else MessageBox.Show("Такої таблиці не існує");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте введені дані та повторіть спробу" + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void RenameTable_Load(object sender, EventArgs e)
        {
            conn.Open();
            DataTable dt = conn.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tableListCmb.Items.Add(tablename);
            }
            tableListCmb.Items.Remove("sysdiagrams");
            tableListCmb.SelectedIndex = 0;
        }
    }
}
