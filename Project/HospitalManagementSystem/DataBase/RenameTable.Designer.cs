﻿namespace HospitalManagementSystem.DataBase
{
    partial class RenameTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableListCmb = new System.Windows.Forms.ComboBox();
            this.tableListLbl = new System.Windows.Forms.Label();
            this.newTableTxt = new System.Windows.Forms.TextBox();
            this.newTableLbl = new System.Windows.Forms.Label();
            this.renameBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tableListCmb
            // 
            this.tableListCmb.FormattingEnabled = true;
            this.tableListCmb.Location = new System.Drawing.Point(173, 5);
            this.tableListCmb.Name = "tableListCmb";
            this.tableListCmb.Size = new System.Drawing.Size(121, 21);
            this.tableListCmb.TabIndex = 0;
            // 
            // tableListLbl
            // 
            this.tableListLbl.AutoSize = true;
            this.tableListLbl.Location = new System.Drawing.Point(8, 8);
            this.tableListLbl.Name = "tableListLbl";
            this.tableListLbl.Size = new System.Drawing.Size(159, 13);
            this.tableListLbl.TabIndex = 1;
            this.tableListLbl.Text = "Таблиця для перейменування";
            // 
            // newTableTxt
            // 
            this.newTableTxt.Location = new System.Drawing.Point(108, 32);
            this.newTableTxt.Name = "newTableTxt";
            this.newTableTxt.Size = new System.Drawing.Size(186, 20);
            this.newTableTxt.TabIndex = 2;
            // 
            // newTableLbl
            // 
            this.newTableLbl.AutoSize = true;
            this.newTableLbl.Location = new System.Drawing.Point(8, 35);
            this.newTableLbl.Name = "newTableLbl";
            this.newTableLbl.Size = new System.Drawing.Size(94, 13);
            this.newTableLbl.TabIndex = 3;
            this.newTableLbl.Text = "Нова ім\'я таблиці";
            // 
            // renameBtn
            // 
            this.renameBtn.Location = new System.Drawing.Point(102, 58);
            this.renameBtn.Name = "renameBtn";
            this.renameBtn.Size = new System.Drawing.Size(98, 23);
            this.renameBtn.TabIndex = 4;
            this.renameBtn.Text = "Перейменувати";
            this.renameBtn.UseVisualStyleBackColor = true;
            this.renameBtn.Click += new System.EventHandler(this.renameBtn_Click);
            // 
            // RenameTable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 86);
            this.Controls.Add(this.renameBtn);
            this.Controls.Add(this.newTableLbl);
            this.Controls.Add(this.newTableTxt);
            this.Controls.Add(this.tableListLbl);
            this.Controls.Add(this.tableListCmb);
            this.Name = "RenameTable";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Перейменування таблиці";
            this.Load += new System.EventHandler(this.RenameTable_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox tableListCmb;
        private System.Windows.Forms.Label tableListLbl;
        private System.Windows.Forms.TextBox newTableTxt;
        private System.Windows.Forms.Label newTableLbl;
        private System.Windows.Forms.Button renameBtn;
    }
}