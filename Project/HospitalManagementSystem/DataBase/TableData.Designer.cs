﻿namespace HospitalManagementSystem.DataBase
{
    partial class TableData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateResultDGV = new System.Windows.Forms.DataGridView();
            this.tableListLbl = new System.Windows.Forms.Label();
            this.tableListCmb = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dateResultDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // dateResultDGV
            // 
            this.dateResultDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dateResultDGV.Location = new System.Drawing.Point(0, 24);
            this.dateResultDGV.Name = "dateResultDGV";
            this.dateResultDGV.Size = new System.Drawing.Size(521, 254);
            this.dateResultDGV.TabIndex = 0;
            // 
            // tableListLbl
            // 
            this.tableListLbl.AutoSize = true;
            this.tableListLbl.Location = new System.Drawing.Point(12, 2);
            this.tableListLbl.Name = "tableListLbl";
            this.tableListLbl.Size = new System.Drawing.Size(92, 13);
            this.tableListLbl.TabIndex = 3;
            this.tableListLbl.Text = "Оберіть таблицю";
            // 
            // tableListCmb
            // 
            this.tableListCmb.FormattingEnabled = true;
            this.tableListCmb.Location = new System.Drawing.Point(110, -1);
            this.tableListCmb.Name = "tableListCmb";
            this.tableListCmb.Size = new System.Drawing.Size(121, 21);
            this.tableListCmb.TabIndex = 2;
            this.tableListCmb.SelectedIndexChanged += new System.EventHandler(this.tableListCmb_SelectedIndexChanged);
            // 
            // TableData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 278);
            this.Controls.Add(this.tableListLbl);
            this.Controls.Add(this.tableListCmb);
            this.Controls.Add(this.dateResultDGV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "TableData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Table Data";
            this.Load += new System.EventHandler(this.Journal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dateResultDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dateResultDGV;
        private System.Windows.Forms.Label tableListLbl;
        private System.Windows.Forms.ComboBox tableListCmb;
    }
}