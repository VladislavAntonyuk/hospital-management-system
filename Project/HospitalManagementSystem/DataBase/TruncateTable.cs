﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.DataBase
{
    public partial class TruncateTable : Form
    {
        public TruncateTable()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);
        private void renameBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (tableListCmb.Items.Contains(tableListCmb.Text))
                {
                    SqlCommand renameSql = new SqlCommand(@"TRUNCATE TABLE " + tableListCmb.SelectedItem, conn);
                    renameSql.ExecuteNonQuery();
                    GlobalVariables.Journal(conn, string.Format("table {0} truncated successfully", tableListCmb.SelectedItem), "TruncateTable");
                    tableListCmb.SelectedIndex = 0;
                    MessageBox.Show("Очищено");
                }
                else MessageBox.Show("Такої таблиці не існує");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте введені дані та повторіть спробу" + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void RenameTable_Load(object sender, EventArgs e)
        {
            conn.Open();
            DataTable dt = conn.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tableListCmb.Items.Add(tablename);
            }
            tableListCmb.Items.Remove("sysdiagrams");
            tableListCmb.SelectedIndex = 0;
        }
    }
}
