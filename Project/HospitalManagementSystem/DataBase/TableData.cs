﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.DataBase
{
    public partial class TableData : Form
    {
        public TableData()
        {
            InitializeComponent();
        }
        SqlConnection con;
        private void Journal_Load(object sender, EventArgs e)
        {
            con = new SqlConnection(GlobalVariables.ConnectionString);
            con.Open();
            GlobalVariables.Journal(con, "Open TableData", "TableData");
            DataTable dt = con.GetSchema("Tables");
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tableListCmb.Items.Add(tablename);
            }
            tableListCmb.Items.Remove("sysdiagrams");
            tableListCmb.SelectedIndex = 0;
        }

        private void tableListCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand select = new SqlCommand(@"Select * from "+ tableListCmb.SelectedItem, con);
            SqlDataAdapter adap = new SqlDataAdapter(select);
            DataSet ds = new DataSet();
            adap.Fill(ds, tableListCmb.SelectedItem.ToString());
            dateResultDGV.DataSource = ds.Tables[0];
            GlobalVariables.Journal(con, "Open Table " + tableListCmb.SelectedItem, "TableData");

        }
    }
}
