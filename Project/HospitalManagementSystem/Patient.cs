﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalManagementSystem
{
    public partial class Patient : Form
    {
        public Patient()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);
        int i = 0;
        string codePat = "";
        DataSet ds = new DataSet();
        int userCount = 1;

        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void likuvanna(int i)
        {
            SqlCommand patInfo = new SqlCommand("SELECT * FROM Пациент_Лечение WHERE Код_пациента LIKE @codePac", conn);
            patInfo.Parameters.AddWithValue("@codePac", codePat);
            // MessageBox.Show(specInfo.CommandText);

            SqlDataAdapter pat = new SqlDataAdapter(patInfo);
            SqlCommandBuilder Patbuilder = new SqlCommandBuilder(pat);

            pat.Fill(ds, "Пациент_Лечение");
            string codeDia = ds.Tables["Пациент_Лечение"].Rows[i]["Диагноз"].ToString();
            //MessageBox.Show(codeDia);
            SqlCommand diaName = new SqlCommand("SELECT * FROM Диагнозы WHERE Код LIKE @codeDia", conn);
            diaName.Parameters.AddWithValue("@codeDia", codeDia);
            // MessageBox.Show(specInfo.CommandText);

            SqlDataAdapter diagnoz = new SqlDataAdapter(diaName);
            SqlCommandBuilder Diabuilder = new SqlCommandBuilder(diagnoz);

            diagnoz.Fill(ds, "Диагнозы");
            diaValue.Text = ds.Tables["Диагнозы"].Rows[i]["Название"].ToString();
            dataPostValue.Text = ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Поступления_месяц"].ToString() + "/" + ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Поступления_день"].ToString() + "/" + ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Поступления_год"].ToString();
            if (ds.Tables["Пациент_Лечение"].Rows[i]["Результат_лечения"].ToString() != "7")
            {

                dataVipValue.Text = ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Выписки_месяц"].ToString() + "/" + ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Выписки_день"].ToString() + "/" + ds.Tables["Пациент_Лечение"].Rows[i]["Дата_Выписки_год"].ToString();
                TimeSpan ts = Convert.ToDateTime(dataVipValue.Text) - Convert.ToDateTime(dataPostValue.Text);
                dayInHosValue.Text = ts.TotalDays.ToString();
            }
            else {
                dataVipValue.Text = "Ще лікується";
                TimeSpan ts = DateTime.Now - Convert.ToDateTime(dataPostValue.Text);
                dayInHosValue.Text = ((int)ts.TotalDays).ToString();
            }


            SqlCommand palNum = new SqlCommand("SELECT * FROM Палата WHERE Код LIKE @codePal", conn);
            palNum.Parameters.AddWithValue("@codePal", ds.Tables["Пациент_Лечение"].Rows[i]["Палата"].ToString());
            // MessageBox.Show(specInfo.CommandText);
            //int palataIndex = (int)palNum.ExecuteScalar();
            //this.Text = palataIndex.ToString();
            SqlDataAdapter palata = new SqlDataAdapter(palNum);
            SqlCommandBuilder palatabuilder = new SqlCommandBuilder(palata);

            palata.Fill(ds, "Палата");
            palValue.Text = "№" + ds.Tables["Палата"].Rows[i]["Номер_палаты"].ToString() + " " + ds.Tables["Палата"].Rows[i]["Тип_палаты"].ToString();


            SqlCommand otdelNum = new SqlCommand("SELECT * FROM Отделение WHERE Код LIKE @codeOtdel", conn);
            otdelNum.Parameters.AddWithValue("@codeOtdel", ds.Tables["Палата"].Rows[i]["Отделение"].ToString());
            // MessageBox.Show(specInfo.CommandText);

            SqlDataAdapter otdelenie = new SqlDataAdapter(otdelNum);
            SqlCommandBuilder otdelbuilder = new SqlCommandBuilder(otdelenie);

            otdelenie.Fill(ds, "Отделение");
            otdelValue.Text = ds.Tables["Отделение"].Rows[i]["Название"].ToString();




            SqlCommand countPatcmd = new SqlCommand(@"SELECT COUNT(Пациент_Лечение.Код_пациента) AS Expr1
FROM Диагнозы INNER JOIN
                         Пациент_Лечение ON Диагнозы.Код = Пациент_Лечение.Диагноз INNER JOIN
                         Результат_лечения ON Пациент_Лечение.Результат_лечения = Результат_лечения.Код
GROUP BY Пациент_Лечение.Диагноз, Диагнозы.Название
HAVING(Пациент_Лечение.Диагноз = @codeDia)", conn);
            countPatcmd.Parameters.AddWithValue("@codeDia", codeDia);
            // MessageBox.Show(specInfo.CommandText);
            int countPat = (int)countPatcmd.ExecuteScalar();


            SqlCommand smertCmd = new SqlCommand(@"SELECT 100*COUNT(Пациент_Лечение.Код_пациента) / @countPat AS Expr2
FROM            Диагнозы INNER JOIN
                         Пациент_Лечение ON Диагнозы.Код = Пациент_Лечение.Диагноз INNER JOIN
                         Результат_лечения ON Пациент_Лечение.Результат_лечения = Результат_лечения.Код
GROUP BY Диагнозы.Название, Результат_лечения.Название
HAVING        (Диагнозы.Название = @diaName AND Результат_лечения.Название=N'Умер')", conn);
            smertCmd.Parameters.AddWithValue("@countPat", countPat - 1);
            smertCmd.Parameters.AddWithValue("@diaName", diaValue.Text);
            // MessageBox.Show(specInfo.CommandText);
            int smertPerc = 0;
            try
            {
                smertPerc = (int)smertCmd.ExecuteScalar();
            }
            catch (Exception)
            {

            }

            smertValue.Text = smertPerc.ToString() + "%";



            SqlCommand lek = new SqlCommand(@"SELECT Лекарства.Название, Лекарства.Описание,  Лекарства.Производитель,Лекарства.Форма_выпуска, Лекарства.Дозировка, Лекарства.Цена
FROM Пациент INNER JOIN(Лекарства INNER JOIN((Диагнозы INNER JOIN Пациент_Лечение ON Диагнозы.Код = Пациент_Лечение.Диагноз) INNER JOIN Схема_лечения ON Диагнозы.Код = Схема_лечения.Диагноз) ON Лекарства.Код = Схема_лечения.Название_лекарства) ON Пациент.Код = Пациент_Лечение.Код_пациента
GROUP BY Лекарства.Название, Лекарства.Описание,Лекарства.Форма_выпуска, Лекарства.Производитель, Лекарства.Дозировка, Лекарства.Цена, Пациент.ФИО, Пациент_Лечение.Диагноз
HAVING (((Пациент.ФИО) = @fio) AND((Пациент_Лечение.Диагноз) = @codeDia));", conn);
            lek.Parameters.AddWithValue("@fio", GlobalVariables.Login);
            lek.Parameters.AddWithValue("@codeDia", codeDia);
            // MessageBox.Show(specInfo.CommandText);

            //SqlDataAdapter lekar = new SqlDataAdapter(lek);
            //SqlCommandBuilder lekbuilder = new SqlCommandBuilder(lekar);

            //lekar.Fill(ds, "Лекарство");

            //medDGV.DataSource = lekar;
            using (var adapter = new SqlDataAdapter(lek))
            {
                var dataTable = new DataTable();
                adapter.Fill(dataTable);
                medDGV.DataSource = dataTable;
            }
            using (var adapter = new SqlDataAdapter(lek))
            {
                var myTable = new DataTable();
                var dataTable = new DataTable();
                adapter.Fill(myTable);
                recLikDGV.DataSource = dataTable;
                dataTable = RemoveDuplicateRows(myTable, "Название");
                recLikDGV.DataSource = dataTable;
                double sum = 0;
                foreach (DataRow dr in dataTable.Rows)
                    sum += Convert.ToDouble(dr[5].ToString());

                priceValue.Text = sum.ToString();
            }


            SqlCommand doc = new SqlCommand(@"SELECT Доктор.ФИО, Доктор.Специализация, Доктор.Квалификация, Доктор.Телефон, [Дата_приема]
FROM Диагнозы INNER JOIN((Палата INNER JOIN(Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты) INNER JOIN Пациент_Лечение ON Палата.Код = Пациент_Лечение.Палата) ON Диагнозы.Код = Пациент_Лечение.Диагноз
GROUP BY Доктор_Палата.Код_Палаты,Доктор.ФИО, Доктор.Специализация, Доктор.Квалификация, Доктор.Телефон, [Дата_приема]
 HAVING (((Доктор_Палата.Код_Палаты) LIKE @codePal));
            ", conn);
           doc.Parameters.AddWithValue("@codePal", ds.Tables["Пациент_Лечение"].Rows[i]["Палата"].ToString());
            //MessageBox.Show(palataIndex.ToString());
            SqlDataAdapter doctor = new SqlDataAdapter(doc);
            SqlCommandBuilder docbuilder = new SqlCommandBuilder(doctor);

            doctor.Fill(ds, "ДокторПац");
            docFioValue.Text = ds.Tables["ДокторПац"].Rows[i]["ФИО"].ToString();

            string codeDocSpec = ds.Tables["ДокторПац"].Rows[i]["Специализация"].ToString();
            SqlCommand docspecInfo = new SqlCommand("SELECT Название FROM Специализация WHERE Код LIKE @spec", conn);
            docspecInfo.Parameters.AddWithValue("@spec", codeDocSpec);
           
            specValue.Text = (string)docspecInfo.ExecuteScalar();
            kvalValue.Text = ds.Tables["ДокторПац"].Rows[i]["Квалификация"].ToString();
            docphoneValue.Text = ds.Tables["ДокторПац"].Rows[i]["Телефон"].ToString();

            DateTime zeroTime = new DateTime(1, 1, 1);
            TimeSpan timestas = DateTime.Now - new DateTime(Convert.ToDateTime(ds.Tables["ДокторПац"].Rows[i]["Дата_приема"].ToString()).Year, Convert.ToDateTime(ds.Tables["ДокторПац"].Rows[i]["Дата_приема"].ToString()).Month, Convert.ToDateTime(ds.Tables["ДокторПац"].Rows[i]["Дата_приема"].ToString()).Day);
            stasValue.Text = ((zeroTime + timestas).Year - 1).ToString();
        }

        private DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }


        private void Patient_Load(object sender, EventArgs e)
        {
            backBtn.Enabled = false;
            conn.Open();
            welcomeLbl.Text = "Добро пожаловать, " + GlobalVariables.Login;

            SqlCommand patInfo = new SqlCommand("SELECT * FROM Пациент WHERE ФИО LIKE @Name;", conn);
            patInfo.Parameters.AddWithValue("@Name", GlobalVariables.Login);


            SqlDataAdapter da = new SqlDataAdapter(patInfo);
            SqlCommandBuilder builder = new SqlCommandBuilder(da);

            da.Fill(ds, "Пациенты");


            codePat = ds.Tables["Пациенты"].Rows[0]["Код"].ToString();
            btdValue.Text = Convert.ToDateTime(ds.Tables["Пациенты"].Rows[0]["Дата_рождения"].ToString()).ToShortDateString();
            telValue.Text = ds.Tables["Пациенты"].Rows[0]["Телефон"].ToString();
            adrValue.Text = ds.Tables["Пациенты"].Rows[0]["Адрес_город"].ToString() + ", " + ds.Tables[0].Rows[0]["Адрес_улица"].ToString();

            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from Пациент_Лечение where Код_пациента like @username", conn))
            {

                sqlCommand.Parameters.AddWithValue("@username", codePat);
                userCount = (int)sqlCommand.ExecuteScalar();
                if (userCount == 1) nextBtn.Enabled = false;

            }
            try
            {
                likuvanna(i);
            }
            catch (Exception)
            {
                this.Size = new Size(455, 180);
                LogoutBtn.Location = new Point(this.Width-LogoutBtn.Width-5, LogoutBtn.Location.Y);
                MessageBox.Show("Ви не лікувались за останні 5 років");
            }
            
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            i--;
            nextBtn.Enabled = true;
            if (i < 1) { i = 0; backBtn.Enabled = false; }
            likuvanna(i);
            GlobalVariables.Journal(conn, "Browse treatment history backward", "Patient");
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {

            i++;
            backBtn.Enabled = true;

            if (i >= userCount - 1) { i = userCount - 1; nextBtn.Enabled = false; }
            likuvanna(i);
            GlobalVariables.Journal(conn, "Browse treatment history forward", "Patient");
        }

        private void actPatSearchBtn_Click(object sender, EventArgs e)
        {
            poiskLik(medDGV);
            GlobalVariables.Journal(conn, "Search medicine", "Patient");
        }
        void poiskLik(DataGridView dg)
        {
            dg.ClearSelection();
            string fio = Interaction.InputBox("Введіть назву", "Пошук ліків", "Панадол");
            for (int i = 0; i < dg.Rows.Count - 1; i++)
            {
                if (dg.Rows[i].Cells[0].Value.ToString().StartsWith(fio)) dg.Rows[i].Selected = true;
            }
            if (dg.SelectedRows.Count == 0) MessageBox.Show("Ліки не знайдено");
        }
        public void saveLik(DataGridView dg,string filename)
        {
            int[] maxLengths = new int[dg.Columns.Count];

            for (int i = 0; i < dg.Columns.Count; i++)
            {
                maxLengths[i] = dg.Columns[i].Name.Length;

                foreach (DataGridViewRow row in dg.Rows)
                {
                    if (row.Cells[i].ToString().Length != 0)
                    {
                        int length = row.Cells[i].ToString().Length;

                        if (length > maxLengths[i])
                        {
                            maxLengths[i] = length;
                        }
                    }
                }
            }

            TextWriter sw = new StreamWriter(filename);
            for (int j = 0; j < dg.Columns.Count; j++)
            {
                sw.Write(dg.Columns[j].Name.PadRight(maxLengths[i] + 2));
                // sw.Write(medDGV.Columns[j].Name.ToString() + "\t");
            }
            sw.WriteLine("");
            sw.WriteLine();
            for (int i = 0; i < dg.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dg.Columns.Count; j++)
                {
                    sw.Write(dg.Rows[i].Cells[j].Value.ToString().PadRight(maxLengths[i] + 2));
                }
                sw.WriteLine("");

            }
            sw.Close();
            MessageBox.Show("Збережено");
        }
        private void saveActiveBtn_Click(object sender, EventArgs e)
        {
            saveMedFD.FileName = "Ліки при " + diaValue.Text;
            saveMedFD.Filter = "txt|*.txt";
            if (saveMedFD.ShowDialog() == DialogResult.OK)
            {
                saveLik(medDGV, saveMedFD.FileName);
                GlobalVariables.Journal(conn, "Sav emedicine to " + saveMedFD.FileName, "Patient");
            }
        }

        private void poiskRecBtn_Click(object sender, EventArgs e)
        {
            poiskLik(recLikDGV);
            GlobalVariables.Journal(conn, "Search recommended medicine" , "Patient");
        }

        private void saveRecBtn_Click(object sender, EventArgs e)
        {
            saveMedFD.FileName = "Рекомендовані ліки при " + diaValue.Text;
            saveMedFD.Filter = "txt|*.txt";
            if (saveMedFD.ShowDialog() == DialogResult.OK)
            {
                saveLik(recLikDGV, saveMedFD.FileName);
                GlobalVariables.Journal(conn, "Save recommended medicine to " + saveMedFD.FileName, "Patient");
            }
            
        }

        private void Patient_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.F1)
            {
                Help hlp = new Help();
                hlp.ShowDialog();
            }
        }

        private void Patient_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVariables.Journal(conn, "Logout", "Patient");
            conn.Close();
            new Login().Show();
        }
    }
}
