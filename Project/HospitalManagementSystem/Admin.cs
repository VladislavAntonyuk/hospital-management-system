﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO;
using Microsoft.Reporting.WinForms;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace HospitalManagementSystem
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }
        #region Translit
        public class Translit
        {
            // объявляем и заполняем словарь с заменами
            // при желании можно исправить словать или дополнить
            Dictionary<string, string> dictionaryChar = new Dictionary<string, string>()
            {
                {"а","a"},
                {"б","b"},
                {"в","v"},
                {"г","g"},
                {"д","d"},
                {"е","e"},
                {"ё","yo"},
                {"ж","zh"},
                {"з","z"},
                {"и","i"},
                {"й","y"},
                {"к","k"},
                {"л","l"},
                {"м","m"},
                {"н","n"},
                {"о","o"},
                {"п","p"},
                {"р","r"},
                {"с","s"},
                {"т","t"},
                {"у","u"},
                {"ф","f"},
                {"х","h"},
                {"ц","ts"},
                {"ч","ch"},
                {"ш","sh"},
                {"щ","sch"},
                {"ъ","'"},
                {"ы","yi"},
                {"ь",""},
                {"э","e"},
                {"ю","yu"},
                {"я","ya"}
            };
            /// <summary>
            /// метод делает транслит на латиницу
            /// </summary>
            /// <param name="source"> это входная строка для транслитерации </param>
            /// <returns>получаем строку после транслитерации</returns>
            public string TranslitFileName(string source)
            {
                var result = "";
                // проход по строке для поиска символов подлежащих замене которые находятся в словаре dictionaryChar
                foreach (var ch in source)
                {
                    var ss = "";
                    // берём каждый символ строки и проверяем его на нахождение его в словаре для замены,
                    // если в словаре есть ключ с таким значением то получаем true 
                    // и добавляем значение из словаря соответствующее ключу
                    if (dictionaryChar.TryGetValue(ch.ToString(), out ss))
                    {
                        result += ss;
                    }
                    // иначе добавляем тот же символ
                    else result += ch;
                }
                return result;
            }
        }

        public static string GetTranslit(string sourceText)
        {
            Translit translit = new Translit();
            // входная строка
            // MessageBox.Show(System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(sourceText.ToLower()));
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(translit.TranslitFileName(sourceText.ToLower()));
        }
        #endregion

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);

        private void Admin_Load(object sender, EventArgs e)
        {
            welcomeLbl.Text = "Добро пожаловать, " + GlobalVariables.ConnectionString.Substring(GlobalVariables.ConnectionString.IndexOf("=") + 1, GlobalVariables.ConnectionString.IndexOf(";") - GlobalVariables.ConnectionString.IndexOf("=") - 1);
            conn.Open();
        }

        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
        public static bool IsAllLetters(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsLetter(c))
                    return false;
            }
            return true;
        }
        private void addDoc_Click(object sender, EventArgs e)
        {
            SqlCommand addDoc = new SqlCommand(@"INSERT INTO Доктор ( ФИО, Пол, Дата_рождения, Дата_приема, Телефон, Адрес_город, Адрес_улица, Специализация, Квалификация)
SELECT @name AS Expr1, @sex AS Expr2, @bd AS Expr3, @priem AS Exp7, @phone AS Expr4, @city AS Expr5, @adr AS Expr6, @spec, @kval;
            ", conn);
            string fio = Interaction.InputBox("Введіть ПІБ", "Додавання лікаря", "Антонов Сергей Владимирович");
            addDoc.Parameters.AddWithValue("@name", fio);
            if (Interaction.InputBox("Введіть стать", "Додавання лікаря", "м").ToString() == "м")
            {
                addDoc.Parameters.AddWithValue("@sex", "м");
            }
            else
                addDoc.Parameters.AddWithValue("@sex", "ж");
            DateTime bd = new DateTime();
        bd:
            try
            {
                bd = Convert.ToDateTime(Interaction.InputBox("Введіть дату народження місяць/день/рік", "Додавання лікаря", DateTime.Now.ToShortDateString()));
                if (bd.Year < DateTime.Now.Year - 18)
                {
                    addDoc.Parameters.AddWithValue("@bd", bd.ToShortDateString());
                }
                else goto bd;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте дату" + Environment.NewLine + ex.Message.ToString());
                goto bd;
            }

            DateTime priem = new DateTime();
        priem: try
            {
                priem = Convert.ToDateTime(Interaction.InputBox("Введіть дату приему на роботу місяць/день/рік", "Додавання лікаря", DateTime.Now.ToShortDateString()));
                if (priem < DateTime.Now && priem.Year > bd.Year + 16)
                {
                    addDoc.Parameters.AddWithValue("@priem", priem.ToShortDateString());
                }
                else goto priem;
            }
            catch (Exception)
            {
                goto priem;
            }

        phone: string phone = Interaction.InputBox("Введіть телефон", "Додавання лікаря", "0987654321");
            if (phone.Length == 10 && IsDigitsOnly(phone))
            {
                addDoc.Parameters.AddWithValue("@phone", phone);
            }
            else goto phone;
            city: string city = Interaction.InputBox("Введіть місто", "Додавання лікаря", "Днепропетровск");
            if (IsAllLetters(city))
            {
                addDoc.Parameters.AddWithValue("@city", city);
            }
            else goto city;
            addDoc.Parameters.AddWithValue("@adr", Interaction.InputBox("Введіть вулицю", "Додавання лікаря", "Глинки 17"));


        spec: string special = Interaction.InputBox("Введіть спеціалізацію", "Додавання лікаря", "Хирург");
            SqlCommand specCountInfo = new SqlCommand("SELECT COUNT(*) FROM Специализация WHERE Название LIKE @spec AND (Персонал=1)", conn);
            specCountInfo.Parameters.AddWithValue("@spec", special);
            DataSet ds = new DataSet();
            int specCount = (int)specCountInfo.ExecuteScalar();
            if (specCount > 0)
            {
                SqlCommand specInfo = new SqlCommand("SELECT * FROM Специализация WHERE Название LIKE @spec", conn);
                specInfo.Parameters.AddWithValue("@spec", special);
                SqlDataAdapter spec = new SqlDataAdapter(specInfo);
                SqlCommandBuilder Specbuilder = new SqlCommandBuilder(spec);
                spec.Fill(ds, "Специализация");

                addDoc.Parameters.AddWithValue("@spec", ds.Tables["Специализация"].Rows[0]["Код"].ToString());
            }
            else goto spec;
            addDoc.Parameters.AddWithValue("@kval", Interaction.InputBox("Введіть кваліфікацію", "Додавання лікаря", "Первая категория"));
            try
            {

                addDoc.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Такий користувач вже існує" + Environment.NewLine + ex.Message.ToString());
            }
            SqlCommand codeDoc = new SqlCommand("SELECT * FROM Доктор WHERE ФИО LIKE @fio", conn);
            codeDoc.Parameters.AddWithValue("@fio", fio);
            SqlDataAdapter codeDoctor = new SqlDataAdapter(codeDoc);
            SqlCommandBuilder CodeDocbuilder = new SqlCommandBuilder(codeDoctor);
            codeDoctor.Fill(ds, "Доктор");


            // 1.  create a command object identifying the stored procedure
            SqlCommand docPass = new SqlCommand("[dbo].[CreateDocPass]", conn);
            // 2. set the command object so it knows to execute a stored procedure
            docPass.CommandType = CommandType.StoredProcedure;
            // 3. add parameter to command, which will be passed to the stored procedure
            docPass.Parameters.Add(new SqlParameter("@cod", ds.Tables["Доктор"].Rows[0]["Код"].ToString()));
            docPass.Parameters.Add(new SqlParameter("@fio", GetTranslit(fio)));

            try
            {
                docPass.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Пароль не призначено" + Environment.NewLine + ex.Message.ToString());
            }


            GlobalVariables.Journal(conn, "Add new doc", "Admin");

        otdel: string otdel = Interaction.InputBox("Введіть отделение", "Призначення палати", "Хирургическое");
            SqlCommand otdelCountInfo = new SqlCommand("SELECT COUNT(*) FROM Отделение WHERE Название LIKE @otdel", conn);
            otdelCountInfo.Parameters.AddWithValue("@otdel", otdel);
            int otdelCount = (int)otdelCountInfo.ExecuteScalar();
            if (otdelCount > 0)
            {
                SqlCommand otdelInfo = new SqlCommand("SELECT * FROM Отделение WHERE Название LIKE @otdel", conn);
                otdelInfo.Parameters.AddWithValue("@otdel", otdel);
                SqlDataAdapter otdelenie = new SqlDataAdapter(otdelInfo);
                SqlCommandBuilder otdelbuilder = new SqlCommandBuilder(otdelenie);
                otdelenie.Fill(ds, "Отделение");

                SqlCommand palataInfo = new SqlCommand("SELECT * FROM Палата WHERE Отделение LIKE @otdel", conn);
                palataInfo.Parameters.AddWithValue("@otdel", ds.Tables["Отделение"].Rows[0]["Код"].ToString());
                SqlDataAdapter palata = new SqlDataAdapter(palataInfo);
                SqlCommandBuilder palatabuilder = new SqlCommandBuilder(palata);
                palata.Fill(ds, "Палата");
            }
            else goto otdel;

            SqlCommand adddocPalata = new SqlCommand(@"INSERT INTO Доктор_Палата ( Код_Доктора, Код_Палаты)
SELECT @codeDoc AS Expr1, @codePal AS Expr2;
            ", conn);
            adddocPalata.Parameters.AddWithValue("@codeDoc", ds.Tables["Доктор"].Rows[0]["Код"].ToString());
            adddocPalata.Parameters.AddWithValue("@codePal", ds.Tables["Палата"].Rows[0]["Код"].ToString());
            try
            {

                adddocPalata.ExecuteNonQuery();
                MessageBox.Show("Додано");

                GlobalVariables.Journal(conn, "Add palata for doc", "Admin");
            }
            catch (Exception ex)
            {
                MessageBox.Show(fio + " вже отримав палату" + Environment.NewLine + ex.Message.ToString());

            }

        }

        public static void Write(DataTable dt, string outputFilePath)
        {
            int[] maxLengths = new int[dt.Columns.Count];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                maxLengths[i] = dt.Columns[i].ColumnName.Length;

                foreach (DataRow row in dt.Rows)
                {
                    if (!row.IsNull(i))
                    {
                        int length = row[i].ToString().Length;

                        if (length > maxLengths[i])
                        {
                            maxLengths[i] = length;
                        }
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(outputFilePath, false))
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sw.Write(dt.Columns[i].ColumnName.PadRight(maxLengths[i] + 2));
                }

                sw.WriteLine();
                sw.WriteLine();
                foreach (DataRow row in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (!row.IsNull(i))
                        {
                            sw.Write(row[i].ToString().PadRight(maxLengths[i] + 2));
                        }
                        else
                        {
                            sw.Write(new string(' ', maxLengths[i] + 2));
                        }
                    }

                    sw.WriteLine();
                }

                sw.Close();

            }
            MessageBox.Show("Збережено");
        }

        private void DeleteDocNurse()
        {
            string sqlDelete = @"DROP TABLE На_увольнение;";
            try
            {
                using (var command = new SqlCommand(sqlDelete, conn))
                    command.ExecuteNonQuery();
            }
            catch (Exception)
            {

            }


            SqlCommand delDoctor = new SqlCommand(@"SELECT Доктор.ФИО, Доктор.Дата_рождения, Специализация.Название, Доктор.Квалификация, Персонал.Должность INTO На_увольнение
FROM(Персонал INNER JOIN Специализация ON Персонал.Код = Специализация.Персонал) INNER JOIN Доктор ON Специализация.Код = Доктор.Специализация
WHERE(DATEPART(yyyy,[Дата_приема])<= " + (DateTime.Now.Year - 30) + @");
            ", conn);

            SqlCommand delNurse = new SqlCommand(@"INSERT INTO На_увольнение(ФИО, Дата_рождения, Название, Квалификация, Должность)
SELECT Медсестра.ФИО, Медсестра.Дата_рождения, Специализация.Название, Медсестра.Квалификация, Персонал.Должность
FROM(Персонал INNER JOIN Специализация ON Персонал.Код = Специализация.Персонал) INNER JOIN Медсестра ON Специализация.Код = Медсестра.Специализация
WHERE(DATEPART(yyyy,[Дата_приема])<= " + (DateTime.Now.Year - 30) + @");
            ", conn);


            try
            {

                delDoctor.ExecuteNonQuery();
                delNurse.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка виконання запиту:" + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void delDoc_Click(object sender, EventArgs e)
        {
            GlobalVariables.Dolgnost = "Врач";
            DeleteDocNurse();
            DialogResult result = MessageBox.Show("Зберегти данні?", "Кандидати додані", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                using (var command = new SqlCommand(@"SELECT * FROM На_увольнение where (Должность = @dol);", conn))
                {
                    command.Parameters.AddWithValue("@dol", GlobalVariables.Dolgnost);
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        var myTable = new DataTable();
                        adapter.Fill(myTable);
                        delDlg.FileName = "На увольнение доктор";
                        delDlg.Filter = "txt|*.txt";
                        if (delDlg.ShowDialog() == DialogResult.OK)
                        {
                            Write(myTable, delDlg.FileName);

                            GlobalVariables.Journal(conn, "Save report Doc to delete", "Admin");
                        }
                        Reports.onDelForm docrep = new Reports.onDelForm();
                        docrep.ShowDialog();
                    }
                }
            }

        }
        private void delNurse_Click(object sender, EventArgs e)
        {
            GlobalVariables.Dolgnost = "Медсестра";
            DeleteDocNurse();
            DialogResult result = MessageBox.Show("Зберегти данні?", "Кандидати додані", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                using (var command = new SqlCommand(@"SELECT * FROM На_увольнение where (Должность = @dol);", conn))
                {
                    command.Parameters.AddWithValue("@dol", GlobalVariables.Dolgnost);
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        var myTable = new DataTable();
                        adapter.Fill(myTable);
                        delDlg.FileName = "На увольнение медсестра";
                        delDlg.Filter = "txt|*.txt";
                        if (delDlg.ShowDialog() == DialogResult.OK)
                        {
                            Write(myTable, delDlg.FileName);
                            GlobalVariables.Journal(conn, "Save report Nurse to delete", "Admin");
                        }

                        Reports.onDelForm nurrep = new Reports.onDelForm();
                        nurrep.ShowDialog();
                    }
                }
            }
        }
        private void upDoc_Click(object sender, EventArgs e)
        {
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from Доктор where ФИО like @username", conn))
            {
                string fio = Interaction.InputBox("Введіть ПІБ", "Пошук лікаря", "Антонов Сергей Владимирович");
                sqlCommand.Parameters.AddWithValue("@username", fio);
                int userCount = (int)sqlCommand.ExecuteScalar();
                if (userCount > 0)
                {

                    SqlCommand upDoc = new SqlCommand(@"UPDATE Доктор SET Доктор.Квалификация = @kval
            WHERE((Доктор.ФИО) = @fio);", conn);
                    upDoc.Parameters.AddWithValue("@fio", fio);
                    string kval = Interaction.InputBox("Введіть нову кваліфікацію", "Лікар " + fio, "Первая категория");
                    upDoc.Parameters.AddWithValue("@kval", kval);
                    try
                    {

                        upDoc.ExecuteNonQuery();
                        GlobalVariables.Journal(conn, "Update doc kval", "Admin");
                        MessageBox.Show("У доктора " + fio + " " + kval);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Дані не оновлено" + Environment.NewLine + ex.Message.ToString());
                    }

                }
                else MessageBox.Show("Такого лікаря немає");
            }
        }

        private void upnurse_Click(object sender, EventArgs e)
        {
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from Медсестра where ФИО like @username", conn))
            {
                string fio = Interaction.InputBox("Введіть ПІБ", "Пошук медсестри", "Михлик Светлана Валентиновна");
                sqlCommand.Parameters.AddWithValue("@username", fio);
                int userCount = (int)sqlCommand.ExecuteScalar();
                if (userCount > 0)
                {

                    SqlCommand upDoc = new SqlCommand(@"UPDATE Медсестра SET Медсестра.Квалификация = @kval
            WHERE((Медсестра.ФИО) = @fio);", conn);
                    upDoc.Parameters.AddWithValue("@fio", fio);
                    string kval = Interaction.InputBox("Введіть нову кваліфікацію", "Медсестра " + fio, "Первая категория");
                    upDoc.Parameters.AddWithValue("@kval", kval);
                    try
                    {

                        upDoc.ExecuteNonQuery();
                        GlobalVariables.Journal(conn, "Update nurse kval", "Admin");
                        MessageBox.Show("У медсестри " + fio + " " + kval);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Дані не оновлено" + Environment.NewLine + ex.Message.ToString());
                    }

                }
                else MessageBox.Show("Такої медсестри немає");
            }
        }

        private void addNurse_Click(object sender, EventArgs e)
        {
            SqlCommand addDoc = new SqlCommand(@"INSERT INTO Медсестра ( ФИО, Пол, Дата_рождения, Дата_приема, Телефон, Адрес_город, Адрес_улица, Специализация, Квалификация)
SELECT @name AS Expr1, @sex AS Expr2, @bd AS Expr3, @priem AS Exp7, @phone AS Expr4, @city AS Expr5, @adr AS Expr6, @spec, @kval;
            ", conn);
            string fio = Interaction.InputBox("Введіть ПІБ", "Додавання медсестри", "Антонова Ольга Владимировна");
            addDoc.Parameters.AddWithValue("@name", fio);
            if (Interaction.InputBox("Введіть стать", "Додавання медсестри", "ж").ToString() == "м")
            {
                addDoc.Parameters.AddWithValue("@sex", "м");
            }
            else
                addDoc.Parameters.AddWithValue("@sex", "ж");
            DateTime bd = new DateTime();
        bd:
            try
            {
                bd = Convert.ToDateTime(Interaction.InputBox("Введіть дату народження місяць/день/рік", "Додавання медсестри", DateTime.Now.ToShortDateString()));
                if (bd.Year < DateTime.Now.Year - 18)
                {
                    addDoc.Parameters.AddWithValue("@bd", bd.ToShortDateString());
                }
                else goto bd;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте дату" + Environment.NewLine + ex.Message.ToString());
                goto bd;
            }

            DateTime priem = new DateTime();
        priem: try
            {
                priem = Convert.ToDateTime(Interaction.InputBox("Введіть дату приему на роботу місяць/день/рік", "Додавання медсестри", DateTime.Now.ToShortDateString()));
                if (priem < DateTime.Now && priem.Year > bd.Year + 16)
                {
                    addDoc.Parameters.AddWithValue("@priem", priem.ToShortDateString());
                }
                else goto priem;
            }
            catch (Exception)
            {
                goto priem;
            }

        phone: string phone = Interaction.InputBox("Введіть телефон", "Додавання медсестри", "0987654321");
            if (phone.Length == 10 && IsDigitsOnly(phone))
            {
                addDoc.Parameters.AddWithValue("@phone", phone);
            }
            else goto phone;
            city: string city = Interaction.InputBox("Введіть місто", "Додавання медсестри", "Днепропетровск");
            if (IsAllLetters(city))
            {
                addDoc.Parameters.AddWithValue("@city", city);
            }
            else goto city;
            addDoc.Parameters.AddWithValue("@adr", Interaction.InputBox("Введіть вулицю", "Додавання лікаря", "Глинки 17"));


        spec: string special = Interaction.InputBox("Введіть спеціалізацію", "Додавання медсестри", "Палатная медсестра");
            SqlCommand specCountInfo = new SqlCommand("SELECT COUNT(*) FROM Специализация WHERE Название LIKE @spec AND (Персонал=2)", conn);
            specCountInfo.Parameters.AddWithValue("@spec", special);
            DataSet ds = new DataSet();
            int specCount = (int)specCountInfo.ExecuteScalar();
            if (specCount > 0)
            {
                SqlCommand specInfo = new SqlCommand("SELECT * FROM Специализация WHERE Название LIKE @spec", conn);
                specInfo.Parameters.AddWithValue("@spec", special);
                SqlDataAdapter spec = new SqlDataAdapter(specInfo);
                SqlCommandBuilder Specbuilder = new SqlCommandBuilder(spec);
                spec.Fill(ds, "Специализация");

                addDoc.Parameters.AddWithValue("@spec", ds.Tables["Специализация"].Rows[0]["Код"].ToString());
            }
            else goto spec;
            addDoc.Parameters.AddWithValue("@kval", Interaction.InputBox("Введіть кваліфікацію", "Додавання медсестри", "Первая категория"));

            try
            {

                addDoc.ExecuteNonQuery();
                MessageBox.Show("Додано");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Такий користувач вже існує" + Environment.NewLine + ex.Message.ToString());
            }

            SqlCommand codeDoc = new SqlCommand("SELECT * FROM Медсестра WHERE ФИО LIKE @fio", conn);
            codeDoc.Parameters.AddWithValue("@fio", fio);
            SqlDataAdapter codeDoctor = new SqlDataAdapter(codeDoc);
            SqlCommandBuilder CodeDocbuilder = new SqlCommandBuilder(codeDoctor);
            codeDoctor.Fill(ds, "Медсестра");

            // 1.  create a command object identifying the stored procedure
            SqlCommand docPass = new SqlCommand("[dbo].[CreateNursePass]", conn);
            // 2. set the command object so it knows to execute a stored procedure
            docPass.CommandType = CommandType.StoredProcedure;
            // 3. add parameter to command, which will be passed to the stored procedure
            docPass.Parameters.Add(new SqlParameter("@cod", ds.Tables["Медсестра"].Rows[0]["Код"].ToString()));
            docPass.Parameters.Add(new SqlParameter("@fio", GetTranslit(fio)));

            try
            {
                docPass.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Пароль не призначено" + Environment.NewLine + ex.Message.ToString());
            }
            GlobalVariables.Journal(conn, "Add new nurse", "Admin");
        }

        private void perMed_Click(object sender, EventArgs e)
        {
            using (SqlCommand sqlCommand = new SqlCommand(@"SELECT COUNT(*) from Лекарства where Название like @lek AND Производитель like @proc AND (Дозировка like @doz)", conn))
            {
                string lek = Interaction.InputBox("Введіть Название", "Пошук лекарства", "Панадол");
                sqlCommand.Parameters.AddWithValue("@lek", lek);
                string proc = Interaction.InputBox("Введіть виробника", "Пошук лекарства", "SmithKline Beecham");
                sqlCommand.Parameters.AddWithValue("@proc", proc);
                string doz = Interaction.InputBox("Введіть дозу", "Пошук лекарства", "500 мг/таблетка");
                sqlCommand.Parameters.AddWithValue("@doz", "%" + doz + "%");
                int userCount = (int)sqlCommand.ExecuteScalar();
                if (userCount > 0)
                {

                    SqlCommand upMed = new SqlCommand(@"UPDATE Лекарства SET Цена = @price
            where(Название like @lek AND Производитель like @proc AND Дозировка like @doz);", conn);
                price: string price = Interaction.InputBox("Введіть цену", "Переоцінка лекарства", "70");
                    if (IsDigitsOnly(price)) upMed.Parameters.AddWithValue("@price", price); else goto price;

                    upMed.Parameters.AddWithValue("@lek", lek);
                    upMed.Parameters.AddWithValue("@proc", proc);
                    upMed.Parameters.AddWithValue("@doz", "%" + doz + "%");
                    try
                    {

                        upMed.ExecuteNonQuery();
                        GlobalVariables.Journal(conn, "update med price", "Admin");
                        MessageBox.Show("Лекарство " + lek + " коштує " + price);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Дані не оновлено" + Environment.NewLine + ex.Message.ToString());
                    }

                }
                else MessageBox.Show("Таких ліків немає");
            }
        }

        private void delMed_Click(object sender, EventArgs e)
        {
            SqlCommand delMed = new SqlCommand(@"DELETE FROM Лекарства
WHERE (Лекарства.Производитель=@proc);
", conn);
            string proc = Interaction.InputBox("Введіть виробника", "Пошук виробника", "SmithKline Beecham");
            delMed.Parameters.AddWithValue("@proc", proc);

            try
            {
                delMed.ExecuteNonQuery();
                GlobalVariables.Journal(conn, "delete medicine from " + proc, "Admin");
                MessageBox.Show("Всі ліки фірми " + proc + " видалено");
            }
            catch (Exception)
            {
                MessageBox.Show("Виробник не знайдено");
            }
        }

        private void addMed_Click(object sender, EventArgs e)
        {
            SqlCommand addMed = new SqlCommand(@"INSERT INTO Лекарства(Название, Описание, Цена, Производитель, Форма_выпуска, Дозировка)
SELECT @name AS Expr1, @opis AS Expr2, @price AS Expr3, @proc AS Expr4, @form AS Expr5, @doz AS Expr6;
            ", conn);
            string name = Interaction.InputBox("Введіть Название", "Додавання лекарства", "Панадол");
            addMed.Parameters.AddWithValue("@name", name);
            string opis = Interaction.InputBox("Введіть опис", "Додавання лекарства", "Для послеоперационного обезболивания");
            addMed.Parameters.AddWithValue("@opis", opis);
        price: string price = Interaction.InputBox("Введіть цену", "Додавання лекарства", "70");
            if (IsDigitsOnly(price)) addMed.Parameters.AddWithValue("@price", price); else goto price;
            string proc = Interaction.InputBox("Введіть виробника", "Додавання лекарства", "SmithKline Beecham");
            addMed.Parameters.AddWithValue("@proc", proc);
            string form = Interaction.InputBox("Введіть форму випуска", "Додавання лекарства", "таблетка");
            addMed.Parameters.AddWithValue("@form", form);
            string doz = Interaction.InputBox("Введіть дозу", "Додавання лекарства", "500 мг/таблетка");
            addMed.Parameters.AddWithValue("@doz", doz);

            try
            {
                addMed.ExecuteNonQuery();
                GlobalVariables.Journal(conn, "add new medicine", "Admin");
                MessageBox.Show("Лекарство додано");
            }
            catch (Exception)
            {
                MessageBox.Show("Лекарство вже існує");
            }


            DataSet ds = new DataSet();
            SqlCommand codeLek = new SqlCommand("SELECT * FROM Лекарства WHERE Название LIKE @name And Цена like @price And Производитель Like @proc And Форма_выпуска Like @form And Дозировка Like @doz", conn);
            codeLek.Parameters.AddWithValue("@name", name);
            codeLek.Parameters.AddWithValue("@proc", proc);
            codeLek.Parameters.AddWithValue("@form", form);
            codeLek.Parameters.AddWithValue("@doz", doz);
            codeLek.Parameters.AddWithValue("@price", price);
            SqlDataAdapter codeMed = new SqlDataAdapter(codeLek);
            SqlCommandBuilder CodeDocbuilder = new SqlCommandBuilder(codeMed);
            codeMed.Fill(ds, "Лекарства");


        dianame: string dianame = Interaction.InputBox("Введіть діагноз", "Призначення ліків", "Паховая грыжа");
            SqlCommand diaCountInfo = new SqlCommand("SELECT COUNT(*) FROM Диагнозы WHERE Название LIKE @dia", conn);
            diaCountInfo.Parameters.AddWithValue("@dia", dianame);
            int diaCount = (int)diaCountInfo.ExecuteScalar();
            if (diaCount > 0)
            {
                SqlCommand diaInfo = new SqlCommand("SELECT * FROM Диагнозы WHERE Название LIKE @dia", conn);
                diaInfo.Parameters.AddWithValue("@dia", dianame);
                SqlDataAdapter diagnoz = new SqlDataAdapter(diaInfo);
                SqlCommandBuilder otdelbuilder = new SqlCommandBuilder(diagnoz);
                diagnoz.Fill(ds, "Диагнозы");

            }
            else goto dianame;

            SqlCommand adddocPalata = new SqlCommand(@"INSERT INTO Схема_лечения ( Название_лекарства, Диагноз)
SELECT @codeLek AS Expr1, @codeDia AS Expr2;
            ", conn);
            adddocPalata.Parameters.AddWithValue("@codeLek", ds.Tables["Лекарства"].Rows[0]["Код"].ToString());
            adddocPalata.Parameters.AddWithValue("@codeDia", ds.Tables["Диагнозы"].Rows[0]["Код"].ToString());
            try
            {

                adddocPalata.ExecuteNonQuery();
                GlobalVariables.Journal(conn, "set medicine diagnosis", "Admin");
                MessageBox.Show("Схема лікування додана");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Схема лікування не назначена" + Environment.NewLine + ex.Message.ToString());
            }

        }

        private void PatInVidCountBtn_Click(object sender, EventArgs e)
        {
            Reports.PatInVidCrossTab patInVidCount = new Reports.PatInVidCrossTab();
            patInVidCount.ShowDialog();
        }

        private void addPatBtn_Click(object sender, EventArgs e)
        {

            SqlCommand addDoc = new SqlCommand(@"INSERT INTO Пациент ( ФИО, Пол, Дата_рождения, Телефон, Адрес_город, Адрес_улица )
SELECT @name AS Expr1, @sex AS Expr2, @bd AS Expr3, @phone AS Expr4, @city AS Expr5, @adr AS Expr6;
            ", conn);
            string fio = Interaction.InputBox("Введіть ПІБ", "Додавання пацієнта", "Антонов Сергей Владимирович");
            addDoc.Parameters.AddWithValue("@name", fio);
            if (Interaction.InputBox("Введіть стать", "Додавання пацієнта", "м").ToString() == "м")
            {
                addDoc.Parameters.AddWithValue("@sex", "м");
            }
            else
                addDoc.Parameters.AddWithValue("@sex", "ж");
            DateTime bd = new DateTime();
        bd:
            try
            {
                bd = Convert.ToDateTime(Interaction.InputBox("Введіть дату народження місяць/день/рік", "Додавання пацієнта", DateTime.Now.ToShortDateString()));
                if (bd < DateTime.Now)
                {
                    addDoc.Parameters.AddWithValue("@bd", bd.ToShortDateString());
                }
                else goto bd;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте введену дату" + Environment.NewLine + ex.Message.ToString());
                goto bd;
            }



        phone: string phone = Interaction.InputBox("Введіть телефон", "Додавання пацієнта", "0987654321");
            if (phone.Length == 10 && IsDigitsOnly(phone))
            {
                addDoc.Parameters.AddWithValue("@phone", phone);
            }
            else goto phone;
            city: string city = Interaction.InputBox("Введіть місто", "Додавання пацієнта", "Днепропетровск");
            if (IsAllLetters(city))
            {
                addDoc.Parameters.AddWithValue("@city", city);
            }
            else goto city;
            addDoc.Parameters.AddWithValue("@adr", Interaction.InputBox("Введіть пацієнта", "Додавання лікаря", "Глинки 17"));



            try
            {

                addDoc.ExecuteNonQuery();
                MessageBox.Show("Додано");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Такий користувач вже існує" + Environment.NewLine + ex.Message.ToString());
            }
            DataSet ds = new DataSet();
            SqlCommand codeDoc = new SqlCommand("SELECT * FROM Пациент WHERE ФИО LIKE @fio", conn);
            codeDoc.Parameters.AddWithValue("@fio", fio);
            SqlDataAdapter codeDoctor = new SqlDataAdapter(codeDoc);
            SqlCommandBuilder CodeDocbuilder = new SqlCommandBuilder(codeDoctor);
            codeDoctor.Fill(ds, "Пациент");

            // 1.  create a command object identifying the stored procedure
            SqlCommand docPass = new SqlCommand("[dbo].[CreatePatientPass]", conn);
            // 2. set the command object so it knows to execute a stored procedure
            docPass.CommandType = CommandType.StoredProcedure;
            // 3. add parameter to command, which will be passed to the stored procedure
            docPass.Parameters.Add(new SqlParameter("@cod", ds.Tables["Пациент"].Rows[0]["Код"].ToString()));
            docPass.Parameters.Add(new SqlParameter("@fio", GetTranslit(fio)));

            try
            {
                docPass.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Пароль не призначено" + Environment.NewLine + ex.Message.ToString());
            }
            GlobalVariables.Journal(conn, "add new patient", "Admin");
        }
        void Lechenie(string fio, DateTime bd)
        {
            DataSet ds = new DataSet();
            SqlCommand codeDoc = new SqlCommand("SELECT * FROM Пациент WHERE ФИО LIKE @fio", conn);
            codeDoc.Parameters.AddWithValue("@fio", fio);
            SqlDataAdapter codeDoctor = new SqlDataAdapter(codeDoc);
            SqlCommandBuilder CodeDocbuilder = new SqlCommandBuilder(codeDoctor);
            codeDoctor.Fill(ds, "Пациент");


        otdel: string diagnoz = Interaction.InputBox("Введіть Диагноз", "Призначення лікування", "Катаракта");
            SqlCommand otdelCountInfo = new SqlCommand("SELECT COUNT(*) FROM Диагнозы WHERE Название LIKE @otdel", conn);
            otdelCountInfo.Parameters.AddWithValue("@otdel", diagnoz);
            int otdelCount = (int)otdelCountInfo.ExecuteScalar();
            if (otdelCount > 0)
            {
                SqlCommand otdelInfo = new SqlCommand("SELECT * FROM Диагнозы WHERE Название LIKE @otdel", conn);
                otdelInfo.Parameters.AddWithValue("@otdel", diagnoz);
                SqlDataAdapter otdelenie = new SqlDataAdapter(otdelInfo);
                SqlCommandBuilder otdelbuilder = new SqlCommandBuilder(otdelenie);
                otdelenie.Fill(ds, "Диагнозы");

                SqlCommand palataInfo = new SqlCommand("SELECT * FROM Палата WHERE Отделение LIKE @otdel", conn);
                palataInfo.Parameters.AddWithValue("@otdel", ds.Tables["Диагнозы"].Rows[0]["Отделение"].ToString());
                SqlDataAdapter palata = new SqlDataAdapter(palataInfo);
                SqlCommandBuilder palatabuilder = new SqlCommandBuilder(palata);
                palata.Fill(ds, "Палата");
            }
            else goto otdel;

            SqlCommand adddocPalata = new SqlCommand(@"INSERT INTO Пациент_Лечение ( Код_Пациента, Диагноз,Палата,Дата_Поступления_год,Дата_Поступления_месяц,Дата_Поступления_день,Дата_Выписки_год,Дата_Выписки_месяц,Дата_Выписки_день,Результат_лечения)
SELECT @codeDoc AS Expr1, @dia AS Expr2,@palata,@postY,@postM,@postD,@vipY,@vipM,@vipD,@res;
            ", conn);
            DateTime postD = new DateTime();
        postuplenie:

            try
            {

                postD = Convert.ToDateTime(Interaction.InputBox("Введіть дату поступлення місяць/день/рік", "Додавання пацієнта", DateTime.Now.ToShortDateString()));
                if (postD > bd && postD <= DateTime.Now)
                {
                    adddocPalata.Parameters.AddWithValue("@postY", postD.Year.ToString());
                    adddocPalata.Parameters.AddWithValue("@postM", postD.Month.ToString());
                    adddocPalata.Parameters.AddWithValue("@postD", postD.Day.ToString());

                }
                else goto postuplenie;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте дату" + Environment.NewLine + ex.Message.ToString());
                goto postuplenie;
            }
            DateTime vipD = new DateTime();
        vipiska:
            try
            {
                string date = Interaction.InputBox("Введіть дату выписки місяць/день/рік (0/0/0 - якщо пацієнт ще лікується)", "Додавання пацієнта", DateTime.Now.ToShortDateString());
                if (!date.StartsWith(@"0/0/0"))
                {
                    vipD = Convert.ToDateTime(date);
                    if (vipD < DateTime.Now && vipD >= postD)
                    {
                        adddocPalata.Parameters.AddWithValue("@vipY", vipD.Year.ToString());
                        adddocPalata.Parameters.AddWithValue("@vipM", vipD.Month.ToString());
                        adddocPalata.Parameters.AddWithValue("@vipD", vipD.Day.ToString());
                    }
                    else goto vipiska;
                }
                else
                {
                    adddocPalata.Parameters.AddWithValue("@vipY", "0");
                    adddocPalata.Parameters.AddWithValue("@vipM", "0");
                    adddocPalata.Parameters.AddWithValue("@vipD", "0");
                    adddocPalata.Parameters.AddWithValue("@res", "7");

                    goto konec;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте дату" + Environment.NewLine + ex.Message.ToString());
                goto vipiska;
            }

        resLec: string resultLec = Interaction.InputBox("Введіть результат лечения", "Призначення лікування", "Выздоровление");
            SqlCommand resCountInfo = new SqlCommand("SELECT COUNT(*) FROM Результат_лечения WHERE Название LIKE @otdel", conn);
            resCountInfo.Parameters.AddWithValue("@otdel", resultLec);
            int resCount = (int)resCountInfo.ExecuteScalar();
            if (resCount > 0)
            {
                SqlCommand otdelInfo = new SqlCommand("SELECT * FROM Результат_лечения WHERE Название LIKE @otdel", conn);
                otdelInfo.Parameters.AddWithValue("@otdel", resultLec);
                SqlDataAdapter otdelenie = new SqlDataAdapter(otdelInfo);
                SqlCommandBuilder otdelbuilder = new SqlCommandBuilder(otdelenie);
                otdelenie.Fill(ds, "Результат_лечения");
            }
            else goto resLec;
            adddocPalata.Parameters.AddWithValue("@res", ds.Tables["Результат_лечения"].Rows[0]["Код"].ToString());

        konec:


            adddocPalata.Parameters.AddWithValue("@codeDoc", ds.Tables["Пациент"].Rows[0]["Код"].ToString());
            adddocPalata.Parameters.AddWithValue("@dia", ds.Tables["Диагнозы"].Rows[0]["Код"].ToString());
            adddocPalata.Parameters.AddWithValue("@palata", ds.Tables["Палата"].Rows[0]["Код"].ToString());

            try
            {

                adddocPalata.ExecuteNonQuery();
                MessageBox.Show("Додано");
            }
            catch (Exception ex)
            {
                MessageBox.Show(fio + " вже назначено лікування" + Environment.NewLine + ex.Message.ToString());
            }
        }
        private void lechenieBtn_Click(object sender, EventArgs e)
        {
            string fio = Interaction.InputBox("Введіть ПІБ", "Пошук пацієнта для призначення лікування", "Антонов Сергей Владимирович");
            SqlCommand otdelCountInfo = new SqlCommand("SELECT COUNT(*) FROM Пациент WHERE ФИО LIKE @otdel", conn);
            otdelCountInfo.Parameters.AddWithValue("@otdel", fio);
            int otdelCount = (int)otdelCountInfo.ExecuteScalar();
            if (otdelCount > 0)
            {
                SqlCommand bdinfo = new SqlCommand("SELECT Дата_рождения FROM Пациент WHERE ФИО LIKE @otdel", conn);
                bdinfo.Parameters.AddWithValue("@otdel", fio);
                DateTime bd = (DateTime)bdinfo.ExecuteScalar();
                Lechenie(fio, bd);
                GlobalVariables.Journal(conn, "add patient treatment", "Admin");
            }
        }

        private void backupBtn_Click(object sender, EventArgs e)
        {

            //Connect to the local, default instance of SQL Server.
            delDlg.InitialDirectory = Environment.CurrentDirectory;
            delDlg.FileName = "HospitalManagementSystem.bak";
            delDlg.Filter = "bak|*.bak";
            if (delDlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    File.Delete(delDlg.FileName);
                }
                catch (Exception)
                {
                }
                try
                {


                    Server srv = new Server(new ServerConnection(conn));
                    //Reference the AdventureWorks2012 database.
                    // Database db = new Database(srv, "HospitalManagementSystem");
                    //  db = srv.Databases.("AdventureWorks2012");
                    //Store the current recovery model in a variable.
                    //int recoverymod = 0;
                    //recoverymod = (int)db.DatabaseOptions.RecoveryModel.ToString();
                    ////Define a Backup object variable. 
                    Backup bk = new Backup();
                    //Specify the type of backup, the description, the name, and the database to be backed up.
                    bk.Action = BackupActionType.Database;
                    bk.BackupSetDescription = "Full backup";
                    bk.BackupSetName = "Backup";
                    bk.Database = "HospitalManagementSystem";//backUpDlg.FileName.Substring(backUpDlg.FileName.LastIndexOf(@"\") + 1, backUpDlg.FileName.Length - 5 - backUpDlg.FileName.LastIndexOf(@"\"));
                                                             //Declare a BackupDeviceItem by supplying the backup device file name in the constructor, and the type of device is a file.
                                                             //BackupDeviceItem bdi = new BackupDeviceItem(@"HospitalManagementSystem.bak", DeviceType.File);
                                                             ////Add the device to the Backup object.
                                                             //bk.Devices.Add(bdi);
                    bk.Devices.AddDevice(delDlg.FileName, DeviceType.File);
                    //Set the Incremental property to False to specify that this is a full database backup.
                    bk.Incremental = false;
                    //Set the expiration date.
                    System.DateTime backupdate = new System.DateTime();
                    backupdate = DateAndTime.Today.AddDays(10);
                    bk.ExpirationDate = backupdate;
                    bk.PercentComplete += CompletionStatusInPercent;
                    bk.Complete += Backup_Completed;
                    //Specify that the log must be truncated after the backup is complete.
                    bk.LogTruncation = BackupTruncateLogType.Truncate;
                    //Run SqlBackup to perform the full database backup on the instance of SQL Server.
                    bk.SqlBackup(srv);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }

        }
        private void CompletionStatusInPercent(object sender, PercentCompleteEventArgs args)
        {
            statusLbl.Text = string.Format("Статус: {0}%", args.Percent);
        }
        private void Backup_Completed(object sender, ServerMessageEventArgs args)
        {
            MessageBox.Show("Backup завершено");
            statusLbl.Text = "Статус: 0%";
            GlobalVariables.Journal(conn, "Backup", "Admin");
        }

        private void renameBtn_Click(object sender, EventArgs e)
        {
            DataBase.RenameTable rnm = new DataBase.RenameTable();
            rnm.ShowDialog();
        }


        private void addTableBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string tableName = Interaction.InputBox("Введіть імя таблиці", "Створення таблиці");
                int columnsCount = Convert.ToInt32(Interaction.InputBox("Введіть кількість стовців", "Створення таблиці"));
                string columnsString = "";
                for (int i = 0; i < columnsCount; i++)
                {
                    columnsString += Interaction.InputBox("Введіть імя " + (i + 1) + " стовпця", "Створення таблиці") + " " + Interaction.InputBox("Введіть тип " + (i + 1) + " стовпця", "Створення таблиці") + "," + Environment.NewLine;
                }
                string sql = string.Format(@"
            CREATE TABLE {0}
(
ID int,
{1}
);", tableName, columnsString.Substring(1, columnsString.Length - 4));
                SqlCommand createTable = new SqlCommand(sql, conn);
                createTable.ExecuteNonQuery();

                GlobalVariables.Journal(conn, "create table " + tableName, "Admin");
                MessageBox.Show("Таблицю " + tableName + " створено");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте введені дані" + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void delTableBtn_Click(object sender, EventArgs e)
        {
            DataBase.DeleteTable del = new DataBase.DeleteTable();
            del.ShowDialog();
        }

        private void Admin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Help hlp = new Help(); hlp.ShowDialog();
            }
        }

        private void Admin_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVariables.Journal(conn, "Logout", "Admin");
            conn.Close();
            new Login().Show();
        }

        private void JournalBtn_Click(object sender, EventArgs e)
        {
            new Reports.Journal().ShowDialog();
        }

        private void TruncateBtn_Click(object sender, EventArgs e)
        {
            new DataBase.TruncateTable().ShowDialog();
        }

        private void tableDataBtn_Click(object sender, EventArgs e)
        {
            new DataBase.TableData().ShowDialog();
        }
    }
}
