﻿namespace HospitalManagementSystem
{
    partial class Help
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Help));
            this.helpTabs = new System.Windows.Forms.TabControl();
            this.AdminTab = new System.Windows.Forms.TabPage();
            this.AdminRTB = new System.Windows.Forms.RichTextBox();
            this.DocTab = new System.Windows.Forms.TabPage();
            this.DoctorRTB = new System.Windows.Forms.RichTextBox();
            this.NurseTab = new System.Windows.Forms.TabPage();
            this.NurseRTB = new System.Windows.Forms.RichTextBox();
            this.PatTab = new System.Windows.Forms.TabPage();
            this.PatientRTB = new System.Windows.Forms.RichTextBox();
            this.AboutTab = new System.Windows.Forms.TabPage();
            this.AboutRTB = new System.Windows.Forms.RichTextBox();
            this.helpTabs.SuspendLayout();
            this.AdminTab.SuspendLayout();
            this.DocTab.SuspendLayout();
            this.NurseTab.SuspendLayout();
            this.PatTab.SuspendLayout();
            this.AboutTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // helpTabs
            // 
            this.helpTabs.Controls.Add(this.AdminTab);
            this.helpTabs.Controls.Add(this.DocTab);
            this.helpTabs.Controls.Add(this.NurseTab);
            this.helpTabs.Controls.Add(this.PatTab);
            this.helpTabs.Controls.Add(this.AboutTab);
            this.helpTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpTabs.Location = new System.Drawing.Point(0, 0);
            this.helpTabs.Name = "helpTabs";
            this.helpTabs.SelectedIndex = 0;
            this.helpTabs.Size = new System.Drawing.Size(597, 316);
            this.helpTabs.TabIndex = 0;
            // 
            // AdminTab
            // 
            this.AdminTab.Controls.Add(this.AdminRTB);
            this.AdminTab.Location = new System.Drawing.Point(4, 22);
            this.AdminTab.Name = "AdminTab";
            this.AdminTab.Padding = new System.Windows.Forms.Padding(3);
            this.AdminTab.Size = new System.Drawing.Size(589, 290);
            this.AdminTab.TabIndex = 0;
            this.AdminTab.Text = "Администратор";
            this.AdminTab.UseVisualStyleBackColor = true;
            // 
            // AdminRTB
            // 
            this.AdminRTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdminRTB.Location = new System.Drawing.Point(3, 3);
            this.AdminRTB.Name = "AdminRTB";
            this.AdminRTB.ReadOnly = true;
            this.AdminRTB.Size = new System.Drawing.Size(583, 284);
            this.AdminRTB.TabIndex = 0;
            this.AdminRTB.Text = resources.GetString("AdminRTB.Text");
            // 
            // DocTab
            // 
            this.DocTab.Controls.Add(this.DoctorRTB);
            this.DocTab.Location = new System.Drawing.Point(4, 22);
            this.DocTab.Name = "DocTab";
            this.DocTab.Padding = new System.Windows.Forms.Padding(3);
            this.DocTab.Size = new System.Drawing.Size(589, 290);
            this.DocTab.TabIndex = 1;
            this.DocTab.Text = "Доктор";
            this.DocTab.UseVisualStyleBackColor = true;
            // 
            // DoctorRTB
            // 
            this.DoctorRTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DoctorRTB.Location = new System.Drawing.Point(3, 3);
            this.DoctorRTB.Name = "DoctorRTB";
            this.DoctorRTB.ReadOnly = true;
            this.DoctorRTB.Size = new System.Drawing.Size(583, 284);
            this.DoctorRTB.TabIndex = 0;
            this.DoctorRTB.Text = resources.GetString("DoctorRTB.Text");
            // 
            // NurseTab
            // 
            this.NurseTab.Controls.Add(this.NurseRTB);
            this.NurseTab.Location = new System.Drawing.Point(4, 22);
            this.NurseTab.Name = "NurseTab";
            this.NurseTab.Padding = new System.Windows.Forms.Padding(3);
            this.NurseTab.Size = new System.Drawing.Size(589, 290);
            this.NurseTab.TabIndex = 2;
            this.NurseTab.Text = "Медсестра";
            this.NurseTab.UseVisualStyleBackColor = true;
            // 
            // NurseRTB
            // 
            this.NurseRTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NurseRTB.Location = new System.Drawing.Point(3, 3);
            this.NurseRTB.Name = "NurseRTB";
            this.NurseRTB.ReadOnly = true;
            this.NurseRTB.Size = new System.Drawing.Size(583, 284);
            this.NurseRTB.TabIndex = 0;
            this.NurseRTB.Text = resources.GetString("NurseRTB.Text");
            // 
            // PatTab
            // 
            this.PatTab.Controls.Add(this.PatientRTB);
            this.PatTab.Location = new System.Drawing.Point(4, 22);
            this.PatTab.Name = "PatTab";
            this.PatTab.Padding = new System.Windows.Forms.Padding(3);
            this.PatTab.Size = new System.Drawing.Size(589, 290);
            this.PatTab.TabIndex = 3;
            this.PatTab.Text = "Пацієнт";
            this.PatTab.UseVisualStyleBackColor = true;
            // 
            // PatientRTB
            // 
            this.PatientRTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PatientRTB.Location = new System.Drawing.Point(3, 3);
            this.PatientRTB.Name = "PatientRTB";
            this.PatientRTB.ReadOnly = true;
            this.PatientRTB.Size = new System.Drawing.Size(583, 284);
            this.PatientRTB.TabIndex = 0;
            this.PatientRTB.Text = resources.GetString("PatientRTB.Text");
            // 
            // AboutTab
            // 
            this.AboutTab.Controls.Add(this.AboutRTB);
            this.AboutTab.Location = new System.Drawing.Point(4, 22);
            this.AboutTab.Name = "AboutTab";
            this.AboutTab.Padding = new System.Windows.Forms.Padding(3);
            this.AboutTab.Size = new System.Drawing.Size(589, 290);
            this.AboutTab.TabIndex = 4;
            this.AboutTab.Text = "About";
            this.AboutTab.UseVisualStyleBackColor = true;
            // 
            // AboutRTB
            // 
            this.AboutRTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AboutRTB.Location = new System.Drawing.Point(3, 3);
            this.AboutRTB.Name = "AboutRTB";
            this.AboutRTB.ReadOnly = true;
            this.AboutRTB.Size = new System.Drawing.Size(583, 284);
            this.AboutRTB.TabIndex = 1;
            this.AboutRTB.Text = "Hospital Management System\n\nVladislav Antonyuk\nPZ-14-1\nDnipropetrovsk National Un" +
    "iversity named after Oles Gonchar";
            // 
            // Help
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 316);
            this.Controls.Add(this.helpTabs);
            this.Name = "Help";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Help";
            this.helpTabs.ResumeLayout(false);
            this.AdminTab.ResumeLayout(false);
            this.DocTab.ResumeLayout(false);
            this.NurseTab.ResumeLayout(false);
            this.PatTab.ResumeLayout(false);
            this.AboutTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl helpTabs;
        private System.Windows.Forms.TabPage AdminTab;
        private System.Windows.Forms.RichTextBox AdminRTB;
        private System.Windows.Forms.TabPage DocTab;
        private System.Windows.Forms.RichTextBox DoctorRTB;
        private System.Windows.Forms.TabPage NurseTab;
        private System.Windows.Forms.RichTextBox NurseRTB;
        private System.Windows.Forms.TabPage PatTab;
        private System.Windows.Forms.RichTextBox PatientRTB;
        private System.Windows.Forms.TabPage AboutTab;
        private System.Windows.Forms.RichTextBox AboutRTB;
    }
}