﻿namespace HospitalManagementSystem
{
    partial class Patient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoutBtn = new System.Windows.Forms.Button();
            this.priceValue = new System.Windows.Forms.Label();
            this.licGB = new System.Windows.Forms.GroupBox();
            this.stasValue = new System.Windows.Forms.Label();
            this.stashLbl = new System.Windows.Forms.Label();
            this.docphoneValue = new System.Windows.Forms.Label();
            this.kvalValue = new System.Windows.Forms.Label();
            this.specValue = new System.Windows.Forms.Label();
            this.docFioValue = new System.Windows.Forms.Label();
            this.docphoneLbl = new System.Windows.Forms.Label();
            this.kvalLbl = new System.Windows.Forms.Label();
            this.specLbl = new System.Windows.Forms.Label();
            this.docFioLbl = new System.Windows.Forms.Label();
            this.dataVipValue = new System.Windows.Forms.Label();
            this.dataVipLbl = new System.Windows.Forms.Label();
            this.nextBtn = new System.Windows.Forms.Button();
            this.dataPostValue = new System.Windows.Forms.Label();
            this.backBtn = new System.Windows.Forms.Button();
            this.smertValue = new System.Windows.Forms.Label();
            this.dataPostLbl = new System.Windows.Forms.Label();
            this.smertLbl = new System.Windows.Forms.Label();
            this.dayInHosValue = new System.Windows.Forms.Label();
            this.dayInHosLbl = new System.Windows.Forms.Label();
            this.priceLbl = new System.Windows.Forms.Label();
            this.diaLbl = new System.Windows.Forms.Label();
            this.otdelLbl = new System.Windows.Forms.Label();
            this.palValue = new System.Windows.Forms.Label();
            this.palLbl = new System.Windows.Forms.Label();
            this.otdelValue = new System.Windows.Forms.Label();
            this.diaValue = new System.Windows.Forms.Label();
            this.medDGV = new System.Windows.Forms.DataGridView();
            this.medGB = new System.Windows.Forms.GroupBox();
            this.adrValue = new System.Windows.Forms.Label();
            this.telValue = new System.Windows.Forms.Label();
            this.btdValue = new System.Windows.Forms.Label();
            this.adrLbl = new System.Windows.Forms.Label();
            this.telLbl = new System.Windows.Forms.Label();
            this.drLbl = new System.Windows.Forms.Label();
            this.InfoGrB = new System.Windows.Forms.GroupBox();
            this.welcomeLbl = new System.Windows.Forms.Label();
            this.saveActiveBtn = new System.Windows.Forms.Button();
            this.actPatSearchBtn = new System.Windows.Forms.Button();
            this.recLikGB = new System.Windows.Forms.GroupBox();
            this.recLikDGV = new System.Windows.Forms.DataGridView();
            this.saveRecBtn = new System.Windows.Forms.Button();
            this.poiskRecBtn = new System.Windows.Forms.Button();
            this.saveMedFD = new System.Windows.Forms.SaveFileDialog();
            this.licGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medDGV)).BeginInit();
            this.medGB.SuspendLayout();
            this.InfoGrB.SuspendLayout();
            this.recLikGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recLikDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // LogoutBtn
            // 
            this.LogoutBtn.Location = new System.Drawing.Point(797, 6);
            this.LogoutBtn.Name = "LogoutBtn";
            this.LogoutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogoutBtn.TabIndex = 18;
            this.LogoutBtn.Text = "Вихід";
            this.LogoutBtn.UseVisualStyleBackColor = true;
            this.LogoutBtn.Click += new System.EventHandler(this.LogoutBtn_Click);
            // 
            // priceValue
            // 
            this.priceValue.AutoSize = true;
            this.priceValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priceValue.Location = new System.Drawing.Point(174, 20);
            this.priceValue.Name = "priceValue";
            this.priceValue.Size = new System.Drawing.Size(13, 13);
            this.priceValue.TabIndex = 6;
            this.priceValue.Text = "+";
            // 
            // licGB
            // 
            this.licGB.Controls.Add(this.stasValue);
            this.licGB.Controls.Add(this.stashLbl);
            this.licGB.Controls.Add(this.docphoneValue);
            this.licGB.Controls.Add(this.kvalValue);
            this.licGB.Controls.Add(this.specValue);
            this.licGB.Controls.Add(this.docFioValue);
            this.licGB.Controls.Add(this.docphoneLbl);
            this.licGB.Controls.Add(this.kvalLbl);
            this.licGB.Controls.Add(this.specLbl);
            this.licGB.Controls.Add(this.docFioLbl);
            this.licGB.Controls.Add(this.dataVipValue);
            this.licGB.Controls.Add(this.dataVipLbl);
            this.licGB.Controls.Add(this.nextBtn);
            this.licGB.Controls.Add(this.dataPostValue);
            this.licGB.Controls.Add(this.backBtn);
            this.licGB.Controls.Add(this.smertValue);
            this.licGB.Controls.Add(this.dataPostLbl);
            this.licGB.Controls.Add(this.smertLbl);
            this.licGB.Controls.Add(this.dayInHosValue);
            this.licGB.Controls.Add(this.priceValue);
            this.licGB.Controls.Add(this.dayInHosLbl);
            this.licGB.Controls.Add(this.priceLbl);
            this.licGB.Controls.Add(this.diaLbl);
            this.licGB.Controls.Add(this.otdelLbl);
            this.licGB.Controls.Add(this.palValue);
            this.licGB.Controls.Add(this.palLbl);
            this.licGB.Controls.Add(this.otdelValue);
            this.licGB.Controls.Add(this.diaValue);
            this.licGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.licGB.Location = new System.Drawing.Point(12, 151);
            this.licGB.Name = "licGB";
            this.licGB.Size = new System.Drawing.Size(427, 388);
            this.licGB.TabIndex = 16;
            this.licGB.TabStop = false;
            this.licGB.Text = "Лікування";
            // 
            // stasValue
            // 
            this.stasValue.AutoSize = true;
            this.stasValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stasValue.Location = new System.Drawing.Point(170, 334);
            this.stasValue.Name = "stasValue";
            this.stasValue.Size = new System.Drawing.Size(33, 13);
            this.stasValue.TabIndex = 32;
            this.stasValue.Text = "Стаж";
            // 
            // stashLbl
            // 
            this.stashLbl.AutoSize = true;
            this.stashLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stashLbl.Location = new System.Drawing.Point(8, 334);
            this.stashLbl.Name = "stashLbl";
            this.stashLbl.Size = new System.Drawing.Size(33, 13);
            this.stashLbl.TabIndex = 31;
            this.stashLbl.Text = "Стаж";
            // 
            // docphoneValue
            // 
            this.docphoneValue.AutoSize = true;
            this.docphoneValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.docphoneValue.Location = new System.Drawing.Point(170, 309);
            this.docphoneValue.Name = "docphoneValue";
            this.docphoneValue.Size = new System.Drawing.Size(86, 13);
            this.docphoneValue.TabIndex = 30;
            this.docphoneValue.Text = "Дата рождения";
            // 
            // kvalValue
            // 
            this.kvalValue.AutoSize = true;
            this.kvalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalValue.Location = new System.Drawing.Point(170, 283);
            this.kvalValue.Name = "kvalValue";
            this.kvalValue.Size = new System.Drawing.Size(86, 13);
            this.kvalValue.TabIndex = 29;
            this.kvalValue.Text = "Дата рождения";
            // 
            // specValue
            // 
            this.specValue.AutoSize = true;
            this.specValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specValue.Location = new System.Drawing.Point(170, 253);
            this.specValue.Name = "specValue";
            this.specValue.Size = new System.Drawing.Size(86, 13);
            this.specValue.TabIndex = 28;
            this.specValue.Text = "Дата рождения";
            // 
            // docFioValue
            // 
            this.docFioValue.AutoSize = true;
            this.docFioValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.docFioValue.Location = new System.Drawing.Point(170, 224);
            this.docFioValue.Name = "docFioValue";
            this.docFioValue.Size = new System.Drawing.Size(86, 13);
            this.docFioValue.TabIndex = 27;
            this.docFioValue.Text = "Дата рождения";
            // 
            // docphoneLbl
            // 
            this.docphoneLbl.AutoSize = true;
            this.docphoneLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.docphoneLbl.Location = new System.Drawing.Point(7, 309);
            this.docphoneLbl.Name = "docphoneLbl";
            this.docphoneLbl.Size = new System.Drawing.Size(52, 13);
            this.docphoneLbl.TabIndex = 26;
            this.docphoneLbl.Text = "Телефон";
            // 
            // kvalLbl
            // 
            this.kvalLbl.AutoSize = true;
            this.kvalLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalLbl.Location = new System.Drawing.Point(7, 283);
            this.kvalLbl.Name = "kvalLbl";
            this.kvalLbl.Size = new System.Drawing.Size(82, 13);
            this.kvalLbl.TabIndex = 25;
            this.kvalLbl.Text = "Квалификация";
            // 
            // specLbl
            // 
            this.specLbl.AutoSize = true;
            this.specLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specLbl.Location = new System.Drawing.Point(7, 253);
            this.specLbl.Name = "specLbl";
            this.specLbl.Size = new System.Drawing.Size(86, 13);
            this.specLbl.TabIndex = 24;
            this.specLbl.Text = "Специализация";
            // 
            // docFioLbl
            // 
            this.docFioLbl.AutoSize = true;
            this.docFioLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.docFioLbl.Location = new System.Drawing.Point(7, 224);
            this.docFioLbl.Name = "docFioLbl";
            this.docFioLbl.Size = new System.Drawing.Size(60, 13);
            this.docFioLbl.TabIndex = 23;
            this.docFioLbl.Text = "ПІБ лікаря";
            // 
            // dataVipValue
            // 
            this.dataVipValue.AutoSize = true;
            this.dataVipValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataVipValue.Location = new System.Drawing.Point(174, 172);
            this.dataVipValue.Name = "dataVipValue";
            this.dataVipValue.Size = new System.Drawing.Size(13, 13);
            this.dataVipValue.TabIndex = 22;
            this.dataVipValue.Text = "+";
            // 
            // dataVipLbl
            // 
            this.dataVipLbl.AutoSize = true;
            this.dataVipLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataVipLbl.Location = new System.Drawing.Point(8, 172);
            this.dataVipLbl.Name = "dataVipLbl";
            this.dataVipLbl.Size = new System.Drawing.Size(78, 13);
            this.dataVipLbl.TabIndex = 21;
            this.dataVipLbl.Text = "Дата виписки";
            // 
            // nextBtn
            // 
            this.nextBtn.Location = new System.Drawing.Point(203, 359);
            this.nextBtn.Name = "nextBtn";
            this.nextBtn.Size = new System.Drawing.Size(75, 23);
            this.nextBtn.TabIndex = 20;
            this.nextBtn.Text = "Вперед";
            this.nextBtn.UseVisualStyleBackColor = true;
            this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
            // 
            // dataPostValue
            // 
            this.dataPostValue.AutoSize = true;
            this.dataPostValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataPostValue.Location = new System.Drawing.Point(173, 145);
            this.dataPostValue.Name = "dataPostValue";
            this.dataPostValue.Size = new System.Drawing.Size(13, 13);
            this.dataPostValue.TabIndex = 15;
            this.dataPostValue.Text = "+";
            // 
            // backBtn
            // 
            this.backBtn.Location = new System.Drawing.Point(122, 359);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(75, 23);
            this.backBtn.TabIndex = 19;
            this.backBtn.Text = "Назад";
            this.backBtn.UseVisualStyleBackColor = true;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // smertValue
            // 
            this.smertValue.AutoSize = true;
            this.smertValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.smertValue.Location = new System.Drawing.Point(174, 46);
            this.smertValue.Name = "smertValue";
            this.smertValue.Size = new System.Drawing.Size(13, 13);
            this.smertValue.TabIndex = 8;
            this.smertValue.Text = "+";
            // 
            // dataPostLbl
            // 
            this.dataPostLbl.AutoSize = true;
            this.dataPostLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dataPostLbl.Location = new System.Drawing.Point(7, 145);
            this.dataPostLbl.Name = "dataPostLbl";
            this.dataPostLbl.Size = new System.Drawing.Size(100, 13);
            this.dataPostLbl.TabIndex = 14;
            this.dataPostLbl.Text = "Дата поступления";
            // 
            // smertLbl
            // 
            this.smertLbl.AutoSize = true;
            this.smertLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.smertLbl.Location = new System.Drawing.Point(6, 46);
            this.smertLbl.Name = "smertLbl";
            this.smertLbl.Size = new System.Drawing.Size(161, 13);
            this.smertLbl.TabIndex = 7;
            this.smertLbl.Text = "Смертность від захворювання";
            // 
            // dayInHosValue
            // 
            this.dayInHosValue.AutoSize = true;
            this.dayInHosValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayInHosValue.Location = new System.Drawing.Point(173, 200);
            this.dayInHosValue.Name = "dayInHosValue";
            this.dayInHosValue.Size = new System.Drawing.Size(13, 13);
            this.dayInHosValue.TabIndex = 13;
            this.dayInHosValue.Text = "+";
            // 
            // dayInHosLbl
            // 
            this.dayInHosLbl.AutoSize = true;
            this.dayInHosLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dayInHosLbl.Location = new System.Drawing.Point(7, 200);
            this.dayInHosLbl.Name = "dayInHosLbl";
            this.dayInHosLbl.Size = new System.Drawing.Size(76, 13);
            this.dayInHosLbl.TabIndex = 12;
            this.dayInHosLbl.Text = "Днів в лікарні";
            // 
            // priceLbl
            // 
            this.priceLbl.AutoSize = true;
            this.priceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priceLbl.Location = new System.Drawing.Point(6, 20);
            this.priceLbl.Name = "priceLbl";
            this.priceLbl.Size = new System.Drawing.Size(102, 13);
            this.priceLbl.TabIndex = 0;
            this.priceLbl.Text = "Вартість лікування";
            // 
            // diaLbl
            // 
            this.diaLbl.AutoSize = true;
            this.diaLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.diaLbl.Location = new System.Drawing.Point(7, 69);
            this.diaLbl.Name = "diaLbl";
            this.diaLbl.Size = new System.Drawing.Size(51, 13);
            this.diaLbl.TabIndex = 1;
            this.diaLbl.Text = "Диагноз";
            // 
            // otdelLbl
            // 
            this.otdelLbl.AutoSize = true;
            this.otdelLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.otdelLbl.Location = new System.Drawing.Point(7, 93);
            this.otdelLbl.Name = "otdelLbl";
            this.otdelLbl.Size = new System.Drawing.Size(62, 13);
            this.otdelLbl.TabIndex = 2;
            this.otdelLbl.Text = "Отделение";
            // 
            // palValue
            // 
            this.palValue.AutoSize = true;
            this.palValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.palValue.Location = new System.Drawing.Point(173, 119);
            this.palValue.Name = "palValue";
            this.palValue.Size = new System.Drawing.Size(13, 13);
            this.palValue.TabIndex = 9;
            this.palValue.Text = "+";
            // 
            // palLbl
            // 
            this.palLbl.AutoSize = true;
            this.palLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.palLbl.Location = new System.Drawing.Point(7, 119);
            this.palLbl.Name = "palLbl";
            this.palLbl.Size = new System.Drawing.Size(44, 13);
            this.palLbl.TabIndex = 3;
            this.palLbl.Text = "Палата";
            // 
            // otdelValue
            // 
            this.otdelValue.AutoSize = true;
            this.otdelValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.otdelValue.Location = new System.Drawing.Point(173, 93);
            this.otdelValue.Name = "otdelValue";
            this.otdelValue.Size = new System.Drawing.Size(13, 13);
            this.otdelValue.TabIndex = 8;
            this.otdelValue.Text = "+";
            // 
            // diaValue
            // 
            this.diaValue.AutoSize = true;
            this.diaValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.diaValue.Location = new System.Drawing.Point(173, 69);
            this.diaValue.Name = "diaValue";
            this.diaValue.Size = new System.Drawing.Size(13, 13);
            this.diaValue.TabIndex = 7;
            this.diaValue.Text = "+";
            // 
            // medDGV
            // 
            this.medDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.medDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.medDGV.Location = new System.Drawing.Point(3, 16);
            this.medDGV.Name = "medDGV";
            this.medDGV.Size = new System.Drawing.Size(421, 225);
            this.medDGV.TabIndex = 0;
            // 
            // medGB
            // 
            this.medGB.Controls.Add(this.medDGV);
            this.medGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.medGB.Location = new System.Drawing.Point(445, 295);
            this.medGB.Name = "medGB";
            this.medGB.Size = new System.Drawing.Size(427, 244);
            this.medGB.TabIndex = 17;
            this.medGB.TabStop = false;
            this.medGB.Text = "Ліки";
            // 
            // adrValue
            // 
            this.adrValue.AutoSize = true;
            this.adrValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrValue.Location = new System.Drawing.Point(119, 77);
            this.adrValue.Name = "adrValue";
            this.adrValue.Size = new System.Drawing.Size(13, 13);
            this.adrValue.TabIndex = 11;
            this.adrValue.Text = "+";
            // 
            // telValue
            // 
            this.telValue.AutoSize = true;
            this.telValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telValue.Location = new System.Drawing.Point(119, 48);
            this.telValue.Name = "telValue";
            this.telValue.Size = new System.Drawing.Size(13, 13);
            this.telValue.TabIndex = 10;
            this.telValue.Text = "+";
            // 
            // btdValue
            // 
            this.btdValue.AutoSize = true;
            this.btdValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btdValue.Location = new System.Drawing.Point(119, 20);
            this.btdValue.Name = "btdValue";
            this.btdValue.Size = new System.Drawing.Size(13, 13);
            this.btdValue.TabIndex = 6;
            this.btdValue.Text = "+";
            // 
            // adrLbl
            // 
            this.adrLbl.AutoSize = true;
            this.adrLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrLbl.Location = new System.Drawing.Point(6, 77);
            this.adrLbl.Name = "adrLbl";
            this.adrLbl.Size = new System.Drawing.Size(38, 13);
            this.adrLbl.TabIndex = 5;
            this.adrLbl.Text = "Адрес";
            // 
            // telLbl
            // 
            this.telLbl.AutoSize = true;
            this.telLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telLbl.Location = new System.Drawing.Point(6, 48);
            this.telLbl.Name = "telLbl";
            this.telLbl.Size = new System.Drawing.Size(52, 13);
            this.telLbl.TabIndex = 4;
            this.telLbl.Text = "Телефон";
            // 
            // drLbl
            // 
            this.drLbl.AutoSize = true;
            this.drLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.drLbl.Location = new System.Drawing.Point(6, 20);
            this.drLbl.Name = "drLbl";
            this.drLbl.Size = new System.Drawing.Size(86, 13);
            this.drLbl.TabIndex = 0;
            this.drLbl.Text = "Дата рождения";
            // 
            // InfoGrB
            // 
            this.InfoGrB.Controls.Add(this.adrValue);
            this.InfoGrB.Controls.Add(this.telValue);
            this.InfoGrB.Controls.Add(this.btdValue);
            this.InfoGrB.Controls.Add(this.adrLbl);
            this.InfoGrB.Controls.Add(this.telLbl);
            this.InfoGrB.Controls.Add(this.drLbl);
            this.InfoGrB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoGrB.Location = new System.Drawing.Point(12, 39);
            this.InfoGrB.Name = "InfoGrB";
            this.InfoGrB.Size = new System.Drawing.Size(427, 106);
            this.InfoGrB.TabIndex = 15;
            this.InfoGrB.TabStop = false;
            this.InfoGrB.Text = "Личная информация";
            // 
            // welcomeLbl
            // 
            this.welcomeLbl.AutoSize = true;
            this.welcomeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomeLbl.Location = new System.Drawing.Point(12, 9);
            this.welcomeLbl.Name = "welcomeLbl";
            this.welcomeLbl.Size = new System.Drawing.Size(259, 17);
            this.welcomeLbl.TabIndex = 14;
            this.welcomeLbl.Text = "Добро пожаловать, @doctorname";
            // 
            // saveActiveBtn
            // 
            this.saveActiveBtn.Location = new System.Drawing.Point(796, 287);
            this.saveActiveBtn.Name = "saveActiveBtn";
            this.saveActiveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveActiveBtn.TabIndex = 19;
            this.saveActiveBtn.Text = "Зберегти";
            this.saveActiveBtn.UseVisualStyleBackColor = true;
            this.saveActiveBtn.Click += new System.EventHandler(this.saveActiveBtn_Click);
            // 
            // actPatSearchBtn
            // 
            this.actPatSearchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actPatSearchBtn.Location = new System.Drawing.Point(716, 287);
            this.actPatSearchBtn.Name = "actPatSearchBtn";
            this.actPatSearchBtn.Size = new System.Drawing.Size(75, 23);
            this.actPatSearchBtn.TabIndex = 20;
            this.actPatSearchBtn.Text = "Пошук";
            this.actPatSearchBtn.UseVisualStyleBackColor = true;
            this.actPatSearchBtn.Click += new System.EventHandler(this.actPatSearchBtn_Click);
            // 
            // recLikGB
            // 
            this.recLikGB.Controls.Add(this.recLikDGV);
            this.recLikGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.recLikGB.Location = new System.Drawing.Point(448, 39);
            this.recLikGB.Name = "recLikGB";
            this.recLikGB.Size = new System.Drawing.Size(427, 244);
            this.recLikGB.TabIndex = 18;
            this.recLikGB.TabStop = false;
            this.recLikGB.Text = "Рекомендовані ліки";
            // 
            // recLikDGV
            // 
            this.recLikDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.recLikDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.recLikDGV.Location = new System.Drawing.Point(3, 16);
            this.recLikDGV.Name = "recLikDGV";
            this.recLikDGV.Size = new System.Drawing.Size(421, 225);
            this.recLikDGV.TabIndex = 0;
            // 
            // saveRecBtn
            // 
            this.saveRecBtn.Location = new System.Drawing.Point(796, 31);
            this.saveRecBtn.Name = "saveRecBtn";
            this.saveRecBtn.Size = new System.Drawing.Size(75, 23);
            this.saveRecBtn.TabIndex = 21;
            this.saveRecBtn.Text = "Зберегти";
            this.saveRecBtn.UseVisualStyleBackColor = true;
            this.saveRecBtn.Click += new System.EventHandler(this.saveRecBtn_Click);
            // 
            // poiskRecBtn
            // 
            this.poiskRecBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.poiskRecBtn.Location = new System.Drawing.Point(716, 31);
            this.poiskRecBtn.Name = "poiskRecBtn";
            this.poiskRecBtn.Size = new System.Drawing.Size(75, 23);
            this.poiskRecBtn.TabIndex = 22;
            this.poiskRecBtn.Text = "Пошук";
            this.poiskRecBtn.UseVisualStyleBackColor = true;
            this.poiskRecBtn.Click += new System.EventHandler(this.poiskRecBtn_Click);
            // 
            // Patient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 551);
            this.Controls.Add(this.saveRecBtn);
            this.Controls.Add(this.poiskRecBtn);
            this.Controls.Add(this.recLikGB);
            this.Controls.Add(this.saveActiveBtn);
            this.Controls.Add(this.actPatSearchBtn);
            this.Controls.Add(this.LogoutBtn);
            this.Controls.Add(this.licGB);
            this.Controls.Add(this.medGB);
            this.Controls.Add(this.InfoGrB);
            this.Controls.Add(this.welcomeLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Patient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пациент";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Patient_FormClosing);
            this.Load += new System.EventHandler(this.Patient_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Patient_KeyDown);
            this.licGB.ResumeLayout(false);
            this.licGB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.medDGV)).EndInit();
            this.medGB.ResumeLayout(false);
            this.InfoGrB.ResumeLayout(false);
            this.InfoGrB.PerformLayout();
            this.recLikGB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recLikDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LogoutBtn;
        private System.Windows.Forms.Label priceValue;
        private System.Windows.Forms.GroupBox licGB;
        private System.Windows.Forms.Label priceLbl;
        private System.Windows.Forms.DataGridView medDGV;
        private System.Windows.Forms.GroupBox medGB;
        private System.Windows.Forms.Label adrValue;
        private System.Windows.Forms.Label telValue;
        private System.Windows.Forms.Label palValue;
        private System.Windows.Forms.Label otdelValue;
        private System.Windows.Forms.Label diaValue;
        private System.Windows.Forms.Label btdValue;
        private System.Windows.Forms.Label adrLbl;
        private System.Windows.Forms.Label telLbl;
        private System.Windows.Forms.Label palLbl;
        private System.Windows.Forms.Label otdelLbl;
        private System.Windows.Forms.Label diaLbl;
        private System.Windows.Forms.Label drLbl;
        private System.Windows.Forms.GroupBox InfoGrB;
        private System.Windows.Forms.Label welcomeLbl;
        private System.Windows.Forms.Label smertValue;
        private System.Windows.Forms.Label smertLbl;
        private System.Windows.Forms.Label dayInHosValue;
        private System.Windows.Forms.Label dayInHosLbl;
        private System.Windows.Forms.Button nextBtn;
        private System.Windows.Forms.Label dataPostValue;
        private System.Windows.Forms.Button backBtn;
        private System.Windows.Forms.Label dataPostLbl;
        private System.Windows.Forms.Label dataVipValue;
        private System.Windows.Forms.Label dataVipLbl;
        private System.Windows.Forms.Label stasValue;
        private System.Windows.Forms.Label stashLbl;
        private System.Windows.Forms.Label docphoneValue;
        private System.Windows.Forms.Label kvalValue;
        private System.Windows.Forms.Label specValue;
        private System.Windows.Forms.Label docFioValue;
        private System.Windows.Forms.Label docphoneLbl;
        private System.Windows.Forms.Label kvalLbl;
        private System.Windows.Forms.Label specLbl;
        private System.Windows.Forms.Label docFioLbl;
        private System.Windows.Forms.Button saveActiveBtn;
        private System.Windows.Forms.Button actPatSearchBtn;
        private System.Windows.Forms.GroupBox recLikGB;
        private System.Windows.Forms.DataGridView recLikDGV;
        private System.Windows.Forms.Button saveRecBtn;
        private System.Windows.Forms.Button poiskRecBtn;
        private System.Windows.Forms.SaveFileDialog saveMedFD;
    }
}