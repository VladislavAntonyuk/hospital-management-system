﻿namespace HospitalManagementSystem
{
    partial class Nurse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.adrValue = new System.Windows.Forms.Label();
            this.telValue = new System.Windows.Forms.Label();
            this.kvalValue = new System.Windows.Forms.Label();
            this.specValue = new System.Windows.Forms.Label();
            this.priemValue = new System.Windows.Forms.Label();
            this.adrLbl = new System.Windows.Forms.Label();
            this.telLbl = new System.Windows.Forms.Label();
            this.kvalLbl = new System.Windows.Forms.Label();
            this.specLbl = new System.Windows.Forms.Label();
            this.priemLbl = new System.Windows.Forms.Label();
            this.drLbl = new System.Windows.Forms.Label();
            this.InfoGrB = new System.Windows.Forms.GroupBox();
            this.btdValue = new System.Windows.Forms.Label();
            this.welcomeLbl = new System.Windows.Forms.Label();
            this.LogoutBtn = new System.Windows.Forms.Button();
            this.InfoGrB.SuspendLayout();
            this.SuspendLayout();
            // 
            // adrValue
            // 
            this.adrValue.AutoSize = true;
            this.adrValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrValue.Location = new System.Drawing.Point(119, 157);
            this.adrValue.Name = "adrValue";
            this.adrValue.Size = new System.Drawing.Size(212, 13);
            this.adrValue.TabIndex = 11;
            this.adrValue.Text = "Днепропетровск, ул. Джержинского, 75";
            // 
            // telValue
            // 
            this.telValue.AutoSize = true;
            this.telValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telValue.Location = new System.Drawing.Point(119, 132);
            this.telValue.Name = "telValue";
            this.telValue.Size = new System.Drawing.Size(86, 13);
            this.telValue.TabIndex = 10;
            this.telValue.Text = "Дата рождения";
            // 
            // kvalValue
            // 
            this.kvalValue.AutoSize = true;
            this.kvalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalValue.Location = new System.Drawing.Point(119, 106);
            this.kvalValue.Name = "kvalValue";
            this.kvalValue.Size = new System.Drawing.Size(86, 13);
            this.kvalValue.TabIndex = 9;
            this.kvalValue.Text = "Дата рождения";
            // 
            // specValue
            // 
            this.specValue.AutoSize = true;
            this.specValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specValue.Location = new System.Drawing.Point(119, 76);
            this.specValue.Name = "specValue";
            this.specValue.Size = new System.Drawing.Size(86, 13);
            this.specValue.TabIndex = 8;
            this.specValue.Text = "Дата рождения";
            // 
            // priemValue
            // 
            this.priemValue.AutoSize = true;
            this.priemValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priemValue.Location = new System.Drawing.Point(119, 47);
            this.priemValue.Name = "priemValue";
            this.priemValue.Size = new System.Drawing.Size(86, 13);
            this.priemValue.TabIndex = 7;
            this.priemValue.Text = "Дата рождения";
            // 
            // adrLbl
            // 
            this.adrLbl.AutoSize = true;
            this.adrLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrLbl.Location = new System.Drawing.Point(6, 157);
            this.adrLbl.Name = "adrLbl";
            this.adrLbl.Size = new System.Drawing.Size(38, 13);
            this.adrLbl.TabIndex = 5;
            this.adrLbl.Text = "Адрес";
            // 
            // telLbl
            // 
            this.telLbl.AutoSize = true;
            this.telLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telLbl.Location = new System.Drawing.Point(6, 132);
            this.telLbl.Name = "telLbl";
            this.telLbl.Size = new System.Drawing.Size(52, 13);
            this.telLbl.TabIndex = 4;
            this.telLbl.Text = "Телефон";
            // 
            // kvalLbl
            // 
            this.kvalLbl.AutoSize = true;
            this.kvalLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalLbl.Location = new System.Drawing.Point(6, 106);
            this.kvalLbl.Name = "kvalLbl";
            this.kvalLbl.Size = new System.Drawing.Size(82, 13);
            this.kvalLbl.TabIndex = 3;
            this.kvalLbl.Text = "Квалификация";
            // 
            // specLbl
            // 
            this.specLbl.AutoSize = true;
            this.specLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specLbl.Location = new System.Drawing.Point(6, 76);
            this.specLbl.Name = "specLbl";
            this.specLbl.Size = new System.Drawing.Size(86, 13);
            this.specLbl.TabIndex = 2;
            this.specLbl.Text = "Специализация";
            // 
            // priemLbl
            // 
            this.priemLbl.AutoSize = true;
            this.priemLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priemLbl.Location = new System.Drawing.Point(6, 47);
            this.priemLbl.Name = "priemLbl";
            this.priemLbl.Size = new System.Drawing.Size(74, 13);
            this.priemLbl.TabIndex = 1;
            this.priemLbl.Text = "Дата приема";
            // 
            // drLbl
            // 
            this.drLbl.AutoSize = true;
            this.drLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.drLbl.Location = new System.Drawing.Point(6, 20);
            this.drLbl.Name = "drLbl";
            this.drLbl.Size = new System.Drawing.Size(86, 13);
            this.drLbl.TabIndex = 0;
            this.drLbl.Text = "Дата рождения";
            // 
            // InfoGrB
            // 
            this.InfoGrB.Controls.Add(this.adrValue);
            this.InfoGrB.Controls.Add(this.telValue);
            this.InfoGrB.Controls.Add(this.kvalValue);
            this.InfoGrB.Controls.Add(this.specValue);
            this.InfoGrB.Controls.Add(this.priemValue);
            this.InfoGrB.Controls.Add(this.btdValue);
            this.InfoGrB.Controls.Add(this.adrLbl);
            this.InfoGrB.Controls.Add(this.telLbl);
            this.InfoGrB.Controls.Add(this.kvalLbl);
            this.InfoGrB.Controls.Add(this.specLbl);
            this.InfoGrB.Controls.Add(this.priemLbl);
            this.InfoGrB.Controls.Add(this.drLbl);
            this.InfoGrB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoGrB.Location = new System.Drawing.Point(12, 37);
            this.InfoGrB.Name = "InfoGrB";
            this.InfoGrB.Size = new System.Drawing.Size(511, 180);
            this.InfoGrB.TabIndex = 14;
            this.InfoGrB.TabStop = false;
            this.InfoGrB.Text = "Личная информация";
            // 
            // btdValue
            // 
            this.btdValue.AutoSize = true;
            this.btdValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btdValue.Location = new System.Drawing.Point(119, 20);
            this.btdValue.Name = "btdValue";
            this.btdValue.Size = new System.Drawing.Size(86, 13);
            this.btdValue.TabIndex = 6;
            this.btdValue.Text = "Дата рождения";
            // 
            // welcomeLbl
            // 
            this.welcomeLbl.AutoSize = true;
            this.welcomeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomeLbl.Location = new System.Drawing.Point(12, 7);
            this.welcomeLbl.Name = "welcomeLbl";
            this.welcomeLbl.Size = new System.Drawing.Size(401, 17);
            this.welcomeLbl.TabIndex = 13;
            this.welcomeLbl.Text = "Добро пожаловать, Михлик Светлана Валентиновна";
            // 
            // LogoutBtn
            // 
            this.LogoutBtn.Location = new System.Drawing.Point(448, 7);
            this.LogoutBtn.Name = "LogoutBtn";
            this.LogoutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogoutBtn.TabIndex = 15;
            this.LogoutBtn.Text = "Вихід";
            this.LogoutBtn.UseVisualStyleBackColor = true;
            this.LogoutBtn.Click += new System.EventHandler(this.LogoutBtn_Click);
            // 
            // Nurse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 220);
            this.Controls.Add(this.LogoutBtn);
            this.Controls.Add(this.InfoGrB);
            this.Controls.Add(this.welcomeLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Nurse";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Медсестра";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Nurse_FormClosing);
            this.Load += new System.EventHandler(this.Nurse_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nurse_KeyDown);
            this.InfoGrB.ResumeLayout(false);
            this.InfoGrB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label adrValue;
        private System.Windows.Forms.Label telValue;
        private System.Windows.Forms.Label kvalValue;
        private System.Windows.Forms.Label specValue;
        private System.Windows.Forms.Label priemValue;
        private System.Windows.Forms.Label adrLbl;
        private System.Windows.Forms.Label telLbl;
        private System.Windows.Forms.Label kvalLbl;
        private System.Windows.Forms.Label specLbl;
        private System.Windows.Forms.Label priemLbl;
        private System.Windows.Forms.Label drLbl;
        private System.Windows.Forms.GroupBox InfoGrB;
        private System.Windows.Forms.Label btdValue;
        private System.Windows.Forms.Label welcomeLbl;
        private System.Windows.Forms.Button LogoutBtn;
    }
}