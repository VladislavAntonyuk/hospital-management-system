﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalManagementSystem
{
    public class GlobalVariables
    {
        public static string connectionString = @"Data Source=VLADISLAV-PC;Initial Catalog=HospitalManagementSystem;Integrated Security=True";//VLADISLAV-PC    //  SVETLANA-LAPTOP\SQLEXPRESS

        public static string ConnectionString
        {
            get { return connectionString; }
            set { connectionString = @"Data Source=" + value + @";Initial Catalog=HospitalManagementSystem;Integrated Security=True"; }
        }

        private static string login = "";
        public static string Login
        {
            get { return login; }
            set { login = value; }
        }

        private static string dolg = "";
        public static string Dolgnost
        {
            get { return dolg; }
            set { dolg = value; }
        }
        public static void Journal(SqlConnection conn, string query,string form)
        {
            SqlCommand journal = new SqlCommand(@"INSERT INTO Журнал ( ФИО, Запрос, Форма, Дата)
SELECT @fio,@query,@table,@data;", conn);
            journal.Parameters.AddWithValue("@fio", Login);
            journal.Parameters.AddWithValue("@query", query);
            journal.Parameters.AddWithValue("@table", form);
            journal.Parameters.AddWithValue("@data", DateTime.Now);
            journal.ExecuteNonQuery();
        }
    }
    
}
