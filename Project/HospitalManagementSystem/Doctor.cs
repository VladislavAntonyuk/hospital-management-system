﻿using HospitalManagementSystem.Reports;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalManagementSystem
{
    public partial class Doctor : Form
    {
        public Doctor()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);
        private void Doctor_Load(object sender, EventArgs e)
        {
            conn.Open();
            welcomeLbl.Text = "Добро пожаловать, " + GlobalVariables.Login;

            SqlCommand doctorInfo = new SqlCommand("SELECT * FROM Доктор WHERE ФИО LIKE @Name;", conn);
            doctorInfo.Parameters.AddWithValue("@Name", GlobalVariables.Login);


            SqlDataAdapter da = new SqlDataAdapter(doctorInfo);
            SqlCommandBuilder builder = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Доктор");

            SqlCommand specInfo = new SqlCommand("SELECT Название FROM Специализация WHERE Код LIKE @spec", conn);
            specInfo.Parameters.AddWithValue("@spec", ds.Tables[0].Rows[0]["Специализация"].ToString());


            btdValue.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_рождения"].ToString()).ToShortDateString();
            priemValue.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_приема"].ToString()).ToShortDateString();
            specValue.Text = (string)specInfo.ExecuteScalar();
            kvalValue.Text = ds.Tables[0].Rows[0]["Квалификация"].ToString();
            telValue.Text = ds.Tables[0].Rows[0]["Телефон"].ToString();
            adrValue.Text = ds.Tables[0].Rows[0]["Адрес_город"].ToString() + ", " + ds.Tables[0].Rows[0]["Адрес_улица"].ToString();
            DateTime zeroTime = new DateTime(1, 1, 1);
            TimeSpan ts = DateTime.Now - new DateTime(Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_приема"].ToString()).Year, Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_приема"].ToString()).Month, Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_приема"].ToString()).Day);
            stasValue.Text = ((zeroTime + ts).Year - 1).ToString();


            //current patients of this doctor
            SqlCommand patients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица, Диагнозы.Название AS Диагноз, DateFromParts([Дата_Поступления_год],[Дата_Поступления_месяц],[Дата_Поступления_день]) AS Дата_Поступления
FROM Результат_лечения INNER JOIN (Диагнозы INNER JOIN (Пациент INNER JOIN ((Палата INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты) INNER JOIN Пациент_Лечение ON Палата.Код = Пациент_Лечение.Палата) ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Диагнозы.Код = Пациент_Лечение.Диагноз) ON Результат_лечения.Код = Пациент_Лечение.Результат_лечения
GROUP BY Доктор.ФИО,Пациент_Лечение.Результат_лечения,Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица, Диагнозы.Название, [Дата_Поступления_год],[Дата_Поступления_месяц],[Дата_Поступления_день]
HAVING (((Доктор.ФИО) LIKE @DocName AND Пациент_Лечение.Результат_лечения = 7));", conn);
            patients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);

            using (var adapter = new SqlDataAdapter(patients))
            {
                var myTable = new DataTable();
                adapter.Fill(myTable);
                curpatientDGV.DataSource = myTable;
            }

            //all patients of this doctor
            SqlCommand allpatients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица, Диагнозы.Название AS Диагноз, Результат_лечения.Название AS Результат_Лечения, DateFromParts([Дата_Поступления_год],[Дата_Поступления_месяц],[Дата_Поступления_день]) AS Дата_Поступления, DateFromParts([Дата_Выписки_год],[Дата_Выписки_месяц],[Дата_Выписки_день]) AS Дата_Выписки
FROM Результат_лечения INNER JOIN (Пациент INNER JOIN ((Палата INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты) INNER JOIN (Диагнозы INNER JOIN Пациент_Лечение ON Диагнозы.Код = Пациент_Лечение.Диагноз) ON Палата.Код = Пациент_Лечение.Палата) ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Результат_лечения.Код = Пациент_Лечение.Результат_лечения
GROUP BY Доктор.ФИО, Пациент_Лечение.Результат_лечения,Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица, Диагнозы.Название, Результат_лечения.Название, [Дата_Поступления_год],[Дата_Поступления_месяц],[Дата_Поступления_день], [Дата_Выписки_год],[Дата_Выписки_месяц],[Дата_Выписки_день]
HAVING (((Доктор.ФИО) LIKE @DocName AND Пациент_Лечение.Результат_лечения <> 7));", conn);
            allpatients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);

            using (var adapter = new SqlDataAdapter(allpatients))
            {
                var Table = new DataTable();
                adapter.Fill(Table);
                allPatientDGV.DataSource = Table;
            }


            //number of patients this month
            SqlCommand patientsCount = new SqlCommand(@"SELECT Count(*)
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName AND Пациент_Лечение.Дата_Выписки_год LIKE @thisyear AND Пациент_Лечение.Дата_Выписки_месяц LIKE @thismonth);", conn);
            patientsCount.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            patientsCount.Parameters.AddWithValue("@thisyear", DateTime.Now.Year.ToString());
            patientsCount.Parameters.AddWithValue("@thismonth", DateTime.Now.Month.ToString());
            numPatThisYearValue.Text = ((int)patientsCount.ExecuteScalar()).ToString();

            //nagruzka this year = 12*пациенты которые выписались до этого месяца/номер месяца
            SqlCommand nagruzka = new SqlCommand(@"SELECT Count(*)
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName AND Пациент_Лечение.Дата_Выписки_год LIKE @thisyear AND Пациент_Лечение.Дата_Выписки_месяц <= @thismonth);", conn);
            nagruzka.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            nagruzka.Parameters.AddWithValue("@thisyear", DateTime.Now.Year.ToString());
            nagruzka.Parameters.AddWithValue("@thismonth", DateTime.Now.Month.ToString());
            nagrValue.Text = (12 * (int)nagruzka.ExecuteScalar() / (double)DateTime.Now.Month).ToString();

            //rabotaKoika = количество пациентов / количество коек в отделении
            SqlCommand otdelNazv = new SqlCommand(@"SELECT Отделение.Название
FROM Доктор INNER JOIN ((Отделение INNER JOIN Палата ON Отделение.Код = Палата.Отделение) INNER JOIN Доктор_Палата ON Палата.Код = Доктор_Палата.Код_Палаты) ON Доктор.Код = Доктор_Палата.Код_Доктора
GROUP BY Отделение.Название, Доктор.ФИО
HAVING (((Доктор.ФИО)=@fio));", conn);
            otdelNazv.Parameters.AddWithValue("@fio", GlobalVariables.Login);


            SqlDataAdapter otdel = new SqlDataAdapter(otdelNazv);
            SqlCommandBuilder Otdelbuilder = new SqlCommandBuilder(otdel);
            otdel.Fill(ds, "Отделение");

            otdelValue.Text = ds.Tables["Отделение"].Rows[0]["Название"].ToString();

            SqlCommand sumkoika = new SqlCommand(@"SELECT  SUM(Палата.Количество_коек) AS Expr1
FROM            Отделение INNER JOIN
                         Палата ON Отделение.Код = Палата.Отделение
GROUP BY Отделение.Название
HAVING        (Отделение.Название = @otdel)", conn);
            sumkoika.Parameters.AddWithValue("@otdel", otdelValue.Text);


            SqlCommand koika = new SqlCommand(@"SELECT 1000*Count(Пациент_Лечение.Код_пациента)/@sumkoika AS exp1
FROM (Отделение RIGHT JOIN Палата ON Отделение.Код = Палата.Отделение) LEFT JOIN Пациент_Лечение ON Палата.Код = Пациент_Лечение.Палата
GROUP BY Отделение.Название, Пациент_Лечение.Дата_Выписки_год, Пациент_Лечение.Дата_Выписки_месяц
HAVING (((Отделение.Название)=@otdel) AND ((Пациент_Лечение.Дата_Выписки_год)=@year) AND ((Пациент_Лечение.Дата_Выписки_месяц)=@month));


", conn);
            koika.Parameters.AddWithValue("@otdel", otdelValue.Text);
            koika.Parameters.AddWithValue("@year", DateTime.Now.Year.ToString());
            koika.Parameters.AddWithValue("@month", DateTime.Now.Month.ToString());
            koika.Parameters.AddWithValue("@sumkoika", ((int)sumkoika.ExecuteScalar()).ToString());

            SqlDataAdapter koikaOtdel = new SqlDataAdapter(koika);
            SqlCommandBuilder Otdelbuilder1 = new SqlCommandBuilder(koikaOtdel);
            koikaOtdel.Fill(ds, "ОтделениеKoika");
            try
            {
                koikaWorkValue.Text = (Convert.ToDouble(ds.Tables["ОтделениеKoika"].Rows[0]["exp1"].ToString()) / 1000).ToString();

            }
            catch (Exception)
            {
                koikaWorkValue.Text = "0.0";

            }

            //result lech
            SqlCommand resultLechenie = new SqlCommand(@"SELECT Count(*)
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName AND Пациент_Лечение.Результат_лечения LIKE @res);", conn);
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 1);
            vizdorovValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 2);
            pokrValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 3);
            bezzminValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 4);
            pogirValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 5);
            perevodValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            resultLechenie.Parameters.AddWithValue("@res", 6);
            pomerValue.Text = ((int)resultLechenie.ExecuteScalar()).ToString();
            resultLechenie.Parameters.Clear();
            // resultLechenie.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            //resultLechenie.Parameters.AddWithValue("@res", 7);
            prodValue.Text = (curpatientDGV.RowCount - 1).ToString();// ((int)resultLechenie.ExecuteScalar()).ToString();

            //koikaden  = количество дней в палате віписаніх пациентов в єтом месяце
            // сумма дней/количество пациентов
            SqlCommand koikaDen = new SqlCommand(@"SELECT        DATEDIFF(d, DateFromParts(Пациент_Лечение.Дата_Поступления_год, Пациент_Лечение.Дата_Поступления_месяц, 
                         Пациент_Лечение.Дата_Поступления_день), DateFromParts(Пациент_Лечение.Дата_Выписки_год, Пациент_Лечение.Дата_Выписки_месяц, 
                         Пациент_Лечение.Дата_Выписки_день)) AS Expr1
FROM            Отделение INNER JOIN
                         Палата ON Отделение.Код = Палата.Отделение INNER JOIN
                         Пациент_Лечение ON Палата.Код = Пациент_Лечение.Палата
WHERE        (Пациент_Лечение.Результат_лечения <> 7) AND (Отделение.Название = @otdel) AND (Пациент_Лечение.Дата_Выписки_месяц = @month)", conn);
            koikaDen.Parameters.AddWithValue("@otdel", otdelValue.Text);
            koikaDen.Parameters.AddWithValue("@month", DateAndTime.Now.Month.ToString());

            using (var adapter = new SqlDataAdapter(koikaDen))
            {
                var myTable = new DataTable();
                adapter.Fill(myTable);
                double sum = 0;
                foreach (DataRow dr in myTable.Rows)
                    sum += Convert.ToDouble(dr[0].ToString());

                if (myTable.Rows.Count > 0)
                    koikaDenValue.Text = (sum / myTable.Rows.Count).ToString();
                else
                    koikaDenValue.Text = "0";
            }

        }

        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveActiveBtn_Click(object sender, EventArgs e)
        {
            SqlCommand patients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName AND Пациент_Лечение.Дата_Выписки_год LIKE 0);", conn);
            patients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            saveas(patients, Environment.CurrentDirectory, @"\Report\Активні пацієнти.txt");
            GlobalVariables.Journal(conn, "save active patients", "Doctor");
        }
        void saveas(SqlCommand sql, string path, string file)
        {
            using (var adapter = new SqlDataAdapter(sql))
            {
                var myTable = new DataTable();
                adapter.Fill(myTable);
                Admin.Write(myTable, path + file);
            }
        }
        private void SaveAllBtn_Click(object sender, EventArgs e)
        {
            SqlCommand allpatients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName);", conn);
            allpatients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            saveas(allpatients, Environment.CurrentDirectory, @"\Report\Усі пацієнти.txt");
            GlobalVariables.Journal(conn, "Save patients", "Doctor");
        }

        private void searchPatBtn_Click(object sender, EventArgs e)
        {
            allPatientDGV.ClearSelection();
            string fio = Interaction.InputBox("Введіть ФИО", "Пошук пацієнта", "Иванов Сергей Иванович");
            for (int i = 0; i < allPatientDGV.Rows.Count - 1; i++)
            {
                if (allPatientDGV.Rows[i].Cells[0].Value.ToString().StartsWith(fio)) allPatientDGV.Rows[i].Selected = true;
            }
            if (allPatientDGV.SelectedRows.Count == 0) MessageBox.Show("Нікого не знайдено");
            GlobalVariables.Journal(conn, "search patients", "Doctor");
        }

        private void actPatSearchBtn_Click(object sender, EventArgs e)
        {
            curpatientDGV.ClearSelection();
            string fio = Interaction.InputBox("Введіть ФИО", "Пошук пацієнта", "Иванов Сергей Иванович");
            for (int i = 0; i < curpatientDGV.Rows.Count - 1; i++)
            {
                if (curpatientDGV.Rows[i].Cells[0].Value.ToString().StartsWith(fio)) curpatientDGV.Rows[i].Selected = true;
            }
            if (curpatientDGV.SelectedRows.Count == 0) MessageBox.Show("Нікого не знайдено");
            GlobalVariables.Journal(conn, "search active patients", "Doctor");
        }

        private void zvitDocBtn_Click(object sender, EventArgs e)
        {
            docYearReportbyPatientForm dyr = new docYearReportbyPatientForm();
            dyr.ShowDialog();
        }

        private void saveActiveAsBtn_Click(object sender, EventArgs e)
        {
            SqlCommand patients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName AND Пациент_Лечение.Дата_Выписки_год LIKE 0);", conn);
            patients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            saveAsDialog.FileName = "Активні пацієнти.txt";
            if (saveAsDialog.ShowDialog() == DialogResult.OK)
                saveas(patients, "", saveAsDialog.FileName);
            GlobalVariables.Journal(conn, "Save active patients to " + saveAsDialog.FileName, "Doctor");
        }

        private void saveAllAsBtn_Click(object sender, EventArgs e)
        {
            SqlCommand allpatients = new SqlCommand(@"SELECT Пациент.ФИО, Пациент.Пол, Пациент.Дата_рождения, Пациент.Телефон, Пациент.Адрес_город, Пациент.Адрес_улица
FROM (Палата INNER JOIN(Пациент INNER JOIN Пациент_Лечение ON Пациент.Код = Пациент_Лечение.Код_пациента) ON Палата.Код = Пациент_Лечение.Палата) INNER JOIN (Доктор INNER JOIN Доктор_Палата ON Доктор.Код = Доктор_Палата.Код_Доктора) ON Палата.Код = Доктор_Палата.Код_Палаты
WHERE(Доктор.ФИО LIKE @DocName);", conn);
            allpatients.Parameters.AddWithValue("@DocName", GlobalVariables.Login);
            saveAsDialog.FileName = "Усі пацієнти.txt";
            if (saveAsDialog.ShowDialog() == DialogResult.OK)
                saveas(allpatients, "", saveAsDialog.FileName);
            GlobalVariables.Journal(conn, "Save patients to " + saveAsDialog.FileName, "Doctor");

        }

        private void Doctor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Help hlp = new Help(); hlp.ShowDialog();
            }
        }

        private void Doctor_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVariables.Journal(conn, "Logout", "Doctor");
            conn.Close();
            new Login().Show();

        }
    }
}
