﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalManagementSystem
{
    public partial class Nurse : Form
    {
        public Nurse()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(GlobalVariables.ConnectionString);

        private void Nurse_Load(object sender, EventArgs e)
        {
            conn.Open();
            welcomeLbl.Text = "Добро пожаловать, " + GlobalVariables.Login;

            SqlCommand doctorInfo = new SqlCommand("SELECT * FROM Медсестра WHERE ФИО LIKE @Name;", conn);
            doctorInfo.Parameters.AddWithValue("@Name", "%" + GlobalVariables.Login + "%");


            SqlDataAdapter da = new SqlDataAdapter(doctorInfo);
            SqlCommandBuilder builder = new SqlCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds, "Доктор");

            SqlCommand specInfo = new SqlCommand("SELECT * FROM Специализация WHERE Код LIKE @spec", conn);
            specInfo.Parameters.AddWithValue("@spec", "%" + ds.Tables[0].Rows[0]["Специализация"].ToString() + "%");
            // MessageBox.Show(specInfo.CommandText);

            SqlDataAdapter spec = new SqlDataAdapter(specInfo);
            SqlCommandBuilder Specbuilder = new SqlCommandBuilder(spec);

            spec.Fill(ds, "Специализация");

            btdValue.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_рождения"].ToString()).ToShortDateString();
            priemValue.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Дата_приема"].ToString()).ToShortDateString();
            specValue.Text = ds.Tables[1].Rows[0]["Название"].ToString();
            kvalValue.Text = ds.Tables[0].Rows[0]["Квалификация"].ToString();
            telValue.Text = ds.Tables[0].Rows[0]["Телефон"].ToString();
            adrValue.Text = ds.Tables[0].Rows[0]["Адрес_город"].ToString() + ", " + ds.Tables[0].Rows[0]["Адрес_улица"].ToString();

        }

        private void LogoutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Nurse_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                Help hlp = new Help();
                hlp.ShowDialog();
            }
        }

        private void Nurse_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalVariables.Journal(conn, "Logout", "Nurse");
            conn.Close();
            new Login().Show();
        }
    }
}
