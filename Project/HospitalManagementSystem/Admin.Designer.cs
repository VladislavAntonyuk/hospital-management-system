﻿namespace HospitalManagementSystem
{
    partial class Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogoutBtn = new System.Windows.Forms.Button();
            this.welcomeLbl = new System.Windows.Forms.Label();
            this.InfoGrB = new System.Windows.Forms.GroupBox();
            this.upDoc = new System.Windows.Forms.Button();
            this.delDoc = new System.Windows.Forms.Button();
            this.addDoc = new System.Windows.Forms.Button();
            this.nurseInfo = new System.Windows.Forms.GroupBox();
            this.upnurse = new System.Windows.Forms.Button();
            this.delNurse = new System.Windows.Forms.Button();
            this.addNurse = new System.Windows.Forms.Button();
            this.medInfo = new System.Windows.Forms.GroupBox();
            this.delMed = new System.Windows.Forms.Button();
            this.addMed = new System.Windows.Forms.Button();
            this.perMed = new System.Windows.Forms.Button();
            this.patGB = new System.Windows.Forms.GroupBox();
            this.lechenieBtn = new System.Windows.Forms.Button();
            this.PatInVidCountBtn = new System.Windows.Forms.Button();
            this.addPatBtn = new System.Windows.Forms.Button();
            this.databaseGB = new System.Windows.Forms.GroupBox();
            this.TruncateBtn = new System.Windows.Forms.Button();
            this.JournalBtn = new System.Windows.Forms.Button();
            this.statusLbl = new System.Windows.Forms.Label();
            this.delTableBtn = new System.Windows.Forms.Button();
            this.addTableBtn = new System.Windows.Forms.Button();
            this.renameBtn = new System.Windows.Forms.Button();
            this.backupBtn = new System.Windows.Forms.Button();
            this.delDlg = new System.Windows.Forms.SaveFileDialog();
            this.tableDataBtn = new System.Windows.Forms.Button();
            this.InfoGrB.SuspendLayout();
            this.nurseInfo.SuspendLayout();
            this.medInfo.SuspendLayout();
            this.patGB.SuspendLayout();
            this.databaseGB.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogoutBtn
            // 
            this.LogoutBtn.Location = new System.Drawing.Point(426, 6);
            this.LogoutBtn.Name = "LogoutBtn";
            this.LogoutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogoutBtn.TabIndex = 20;
            this.LogoutBtn.Text = "Вихід";
            this.LogoutBtn.UseVisualStyleBackColor = true;
            this.LogoutBtn.Click += new System.EventHandler(this.LogoutBtn_Click);
            // 
            // welcomeLbl
            // 
            this.welcomeLbl.AutoSize = true;
            this.welcomeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomeLbl.Location = new System.Drawing.Point(12, 9);
            this.welcomeLbl.Name = "welcomeLbl";
            this.welcomeLbl.Size = new System.Drawing.Size(259, 17);
            this.welcomeLbl.TabIndex = 19;
            this.welcomeLbl.Text = "Добро пожаловать, @doctorname";
            // 
            // InfoGrB
            // 
            this.InfoGrB.Controls.Add(this.upDoc);
            this.InfoGrB.Controls.Add(this.delDoc);
            this.InfoGrB.Controls.Add(this.addDoc);
            this.InfoGrB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoGrB.Location = new System.Drawing.Point(15, 49);
            this.InfoGrB.Name = "InfoGrB";
            this.InfoGrB.Size = new System.Drawing.Size(156, 142);
            this.InfoGrB.TabIndex = 21;
            this.InfoGrB.TabStop = false;
            this.InfoGrB.Text = "Відомості про лікарів";
            // 
            // upDoc
            // 
            this.upDoc.Location = new System.Drawing.Point(6, 94);
            this.upDoc.Name = "upDoc";
            this.upDoc.Size = new System.Drawing.Size(132, 40);
            this.upDoc.TabIndex = 24;
            this.upDoc.Text = "Підвищення кваліфікації";
            this.upDoc.UseVisualStyleBackColor = true;
            this.upDoc.Click += new System.EventHandler(this.upDoc_Click);
            // 
            // delDoc
            // 
            this.delDoc.Location = new System.Drawing.Point(6, 48);
            this.delDoc.Name = "delDoc";
            this.delDoc.Size = new System.Drawing.Size(132, 40);
            this.delDoc.TabIndex = 23;
            this.delDoc.Text = "Кандидати на звільнення";
            this.delDoc.UseVisualStyleBackColor = true;
            this.delDoc.Click += new System.EventHandler(this.delDoc_Click);
            // 
            // addDoc
            // 
            this.addDoc.Location = new System.Drawing.Point(6, 19);
            this.addDoc.Name = "addDoc";
            this.addDoc.Size = new System.Drawing.Size(132, 23);
            this.addDoc.TabIndex = 22;
            this.addDoc.Text = "Прием на роботу";
            this.addDoc.UseVisualStyleBackColor = true;
            this.addDoc.Click += new System.EventHandler(this.addDoc_Click);
            // 
            // nurseInfo
            // 
            this.nurseInfo.Controls.Add(this.upnurse);
            this.nurseInfo.Controls.Add(this.delNurse);
            this.nurseInfo.Controls.Add(this.addNurse);
            this.nurseInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nurseInfo.Location = new System.Drawing.Point(177, 49);
            this.nurseInfo.Name = "nurseInfo";
            this.nurseInfo.Size = new System.Drawing.Size(171, 142);
            this.nurseInfo.TabIndex = 25;
            this.nurseInfo.TabStop = false;
            this.nurseInfo.Text = "Відомості про медсестер";
            // 
            // upnurse
            // 
            this.upnurse.Location = new System.Drawing.Point(6, 94);
            this.upnurse.Name = "upnurse";
            this.upnurse.Size = new System.Drawing.Size(159, 40);
            this.upnurse.TabIndex = 24;
            this.upnurse.Text = "Підвищення кваліфікації";
            this.upnurse.UseVisualStyleBackColor = true;
            this.upnurse.Click += new System.EventHandler(this.upnurse_Click);
            // 
            // delNurse
            // 
            this.delNurse.Location = new System.Drawing.Point(6, 48);
            this.delNurse.Name = "delNurse";
            this.delNurse.Size = new System.Drawing.Size(159, 40);
            this.delNurse.TabIndex = 23;
            this.delNurse.Text = "Кандидати на звільнення";
            this.delNurse.UseVisualStyleBackColor = true;
            this.delNurse.Click += new System.EventHandler(this.delNurse_Click);
            // 
            // addNurse
            // 
            this.addNurse.Location = new System.Drawing.Point(6, 19);
            this.addNurse.Name = "addNurse";
            this.addNurse.Size = new System.Drawing.Size(159, 23);
            this.addNurse.TabIndex = 22;
            this.addNurse.Text = "Прием на роботу";
            this.addNurse.UseVisualStyleBackColor = true;
            this.addNurse.Click += new System.EventHandler(this.addNurse_Click);
            // 
            // medInfo
            // 
            this.medInfo.Controls.Add(this.delMed);
            this.medInfo.Controls.Add(this.addMed);
            this.medInfo.Controls.Add(this.perMed);
            this.medInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.medInfo.Location = new System.Drawing.Point(15, 197);
            this.medInfo.Name = "medInfo";
            this.medInfo.Size = new System.Drawing.Size(156, 142);
            this.medInfo.TabIndex = 26;
            this.medInfo.TabStop = false;
            this.medInfo.Text = "Відомості про ліки";
            // 
            // delMed
            // 
            this.delMed.Location = new System.Drawing.Point(6, 94);
            this.delMed.Name = "delMed";
            this.delMed.Size = new System.Drawing.Size(132, 40);
            this.delMed.TabIndex = 24;
            this.delMed.Text = "Зняття з виробництва";
            this.delMed.UseVisualStyleBackColor = true;
            this.delMed.Click += new System.EventHandler(this.delMed_Click);
            // 
            // addMed
            // 
            this.addMed.Location = new System.Drawing.Point(6, 48);
            this.addMed.Name = "addMed";
            this.addMed.Size = new System.Drawing.Size(132, 40);
            this.addMed.TabIndex = 23;
            this.addMed.Text = "Додавання нових ліків";
            this.addMed.UseVisualStyleBackColor = true;
            this.addMed.Click += new System.EventHandler(this.addMed_Click);
            // 
            // perMed
            // 
            this.perMed.Location = new System.Drawing.Point(6, 19);
            this.perMed.Name = "perMed";
            this.perMed.Size = new System.Drawing.Size(132, 23);
            this.perMed.TabIndex = 22;
            this.perMed.Text = "Перерахунок";
            this.perMed.UseVisualStyleBackColor = true;
            this.perMed.Click += new System.EventHandler(this.perMed_Click);
            // 
            // patGB
            // 
            this.patGB.Controls.Add(this.lechenieBtn);
            this.patGB.Controls.Add(this.PatInVidCountBtn);
            this.patGB.Controls.Add(this.addPatBtn);
            this.patGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.patGB.Location = new System.Drawing.Point(177, 197);
            this.patGB.Name = "patGB";
            this.patGB.Size = new System.Drawing.Size(166, 142);
            this.patGB.TabIndex = 25;
            this.patGB.TabStop = false;
            this.patGB.Text = "Відомості про пацієнтів";
            // 
            // lechenieBtn
            // 
            this.lechenieBtn.Location = new System.Drawing.Point(6, 48);
            this.lechenieBtn.Name = "lechenieBtn";
            this.lechenieBtn.Size = new System.Drawing.Size(154, 40);
            this.lechenieBtn.TabIndex = 24;
            this.lechenieBtn.Text = "Призначення лікування";
            this.lechenieBtn.UseVisualStyleBackColor = true;
            this.lechenieBtn.Click += new System.EventHandler(this.lechenieBtn_Click);
            // 
            // PatInVidCountBtn
            // 
            this.PatInVidCountBtn.Location = new System.Drawing.Point(6, 94);
            this.PatInVidCountBtn.Name = "PatInVidCountBtn";
            this.PatInVidCountBtn.Size = new System.Drawing.Size(154, 40);
            this.PatInVidCountBtn.TabIndex = 23;
            this.PatInVidCountBtn.Text = "Кількість пацієнтів у відділенні";
            this.PatInVidCountBtn.UseVisualStyleBackColor = true;
            this.PatInVidCountBtn.Click += new System.EventHandler(this.PatInVidCountBtn_Click);
            // 
            // addPatBtn
            // 
            this.addPatBtn.Location = new System.Drawing.Point(6, 19);
            this.addPatBtn.Name = "addPatBtn";
            this.addPatBtn.Size = new System.Drawing.Size(154, 23);
            this.addPatBtn.TabIndex = 22;
            this.addPatBtn.Text = "Додавання пацієнтів";
            this.addPatBtn.UseVisualStyleBackColor = true;
            this.addPatBtn.Click += new System.EventHandler(this.addPatBtn_Click);
            // 
            // databaseGB
            // 
            this.databaseGB.Controls.Add(this.tableDataBtn);
            this.databaseGB.Controls.Add(this.TruncateBtn);
            this.databaseGB.Controls.Add(this.JournalBtn);
            this.databaseGB.Controls.Add(this.statusLbl);
            this.databaseGB.Controls.Add(this.delTableBtn);
            this.databaseGB.Controls.Add(this.addTableBtn);
            this.databaseGB.Controls.Add(this.renameBtn);
            this.databaseGB.Controls.Add(this.backupBtn);
            this.databaseGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.databaseGB.Location = new System.Drawing.Point(354, 49);
            this.databaseGB.Name = "databaseGB";
            this.databaseGB.Size = new System.Drawing.Size(147, 290);
            this.databaseGB.TabIndex = 25;
            this.databaseGB.TabStop = false;
            this.databaseGB.Text = "Операції з базою";
            // 
            // TruncateBtn
            // 
            this.TruncateBtn.Location = new System.Drawing.Point(5, 140);
            this.TruncateBtn.Name = "TruncateBtn";
            this.TruncateBtn.Size = new System.Drawing.Size(132, 28);
            this.TruncateBtn.TabIndex = 29;
            this.TruncateBtn.Text = "Очистити таблицю";
            this.TruncateBtn.UseVisualStyleBackColor = true;
            this.TruncateBtn.Click += new System.EventHandler(this.TruncateBtn_Click);
            // 
            // JournalBtn
            // 
            this.JournalBtn.Location = new System.Drawing.Point(6, 208);
            this.JournalBtn.Name = "JournalBtn";
            this.JournalBtn.Size = new System.Drawing.Size(132, 28);
            this.JournalBtn.TabIndex = 28;
            this.JournalBtn.Text = "Журнал";
            this.JournalBtn.UseVisualStyleBackColor = true;
            this.JournalBtn.Click += new System.EventHandler(this.JournalBtn_Click);
            // 
            // statusLbl
            // 
            this.statusLbl.AutoSize = true;
            this.statusLbl.Location = new System.Drawing.Point(40, 45);
            this.statusLbl.Name = "statusLbl";
            this.statusLbl.Size = new System.Drawing.Size(71, 13);
            this.statusLbl.TabIndex = 27;
            this.statusLbl.Text = "Статус: 0%";
            // 
            // delTableBtn
            // 
            this.delTableBtn.Location = new System.Drawing.Point(6, 174);
            this.delTableBtn.Name = "delTableBtn";
            this.delTableBtn.Size = new System.Drawing.Size(132, 28);
            this.delTableBtn.TabIndex = 26;
            this.delTableBtn.Text = "Видалити таблицю";
            this.delTableBtn.UseVisualStyleBackColor = true;
            this.delTableBtn.Click += new System.EventHandler(this.delTableBtn_Click);
            // 
            // addTableBtn
            // 
            this.addTableBtn.Location = new System.Drawing.Point(5, 60);
            this.addTableBtn.Name = "addTableBtn";
            this.addTableBtn.Size = new System.Drawing.Size(132, 28);
            this.addTableBtn.TabIndex = 25;
            this.addTableBtn.Text = "Додати таблицю";
            this.addTableBtn.UseVisualStyleBackColor = true;
            this.addTableBtn.Click += new System.EventHandler(this.addTableBtn_Click);
            // 
            // renameBtn
            // 
            this.renameBtn.Location = new System.Drawing.Point(5, 94);
            this.renameBtn.Name = "renameBtn";
            this.renameBtn.Size = new System.Drawing.Size(132, 40);
            this.renameBtn.TabIndex = 23;
            this.renameBtn.Text = "Перейменувати таблицю";
            this.renameBtn.UseVisualStyleBackColor = true;
            this.renameBtn.Click += new System.EventHandler(this.renameBtn_Click);
            // 
            // backupBtn
            // 
            this.backupBtn.Location = new System.Drawing.Point(6, 19);
            this.backupBtn.Name = "backupBtn";
            this.backupBtn.Size = new System.Drawing.Size(132, 23);
            this.backupBtn.TabIndex = 22;
            this.backupBtn.Text = "Backup";
            this.backupBtn.UseVisualStyleBackColor = true;
            this.backupBtn.Click += new System.EventHandler(this.backupBtn_Click);
            // 
            // tableDataBtn
            // 
            this.tableDataBtn.Location = new System.Drawing.Point(5, 242);
            this.tableDataBtn.Name = "tableDataBtn";
            this.tableDataBtn.Size = new System.Drawing.Size(132, 40);
            this.tableDataBtn.TabIndex = 30;
            this.tableDataBtn.Text = "Відобразити таблицю";
            this.tableDataBtn.UseVisualStyleBackColor = true;
            this.tableDataBtn.Click += new System.EventHandler(this.tableDataBtn_Click);
            // 
            // Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 342);
            this.Controls.Add(this.databaseGB);
            this.Controls.Add(this.patGB);
            this.Controls.Add(this.medInfo);
            this.Controls.Add(this.nurseInfo);
            this.Controls.Add(this.InfoGrB);
            this.Controls.Add(this.LogoutBtn);
            this.Controls.Add(this.welcomeLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Администратор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Admin_FormClosing);
            this.Load += new System.EventHandler(this.Admin_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Admin_KeyDown);
            this.InfoGrB.ResumeLayout(false);
            this.nurseInfo.ResumeLayout(false);
            this.medInfo.ResumeLayout(false);
            this.patGB.ResumeLayout(false);
            this.databaseGB.ResumeLayout(false);
            this.databaseGB.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label welcomeLbl;
        private System.Windows.Forms.Button LogoutBtn;
        private System.Windows.Forms.GroupBox InfoGrB;
        private System.Windows.Forms.Button upDoc;
        private System.Windows.Forms.Button delDoc;
        private System.Windows.Forms.Button addDoc;
        private System.Windows.Forms.GroupBox nurseInfo;
        private System.Windows.Forms.Button upnurse;
        private System.Windows.Forms.Button delNurse;
        private System.Windows.Forms.Button addNurse;
        private System.Windows.Forms.GroupBox medInfo;
        private System.Windows.Forms.Button delMed;
        private System.Windows.Forms.Button addMed;
        private System.Windows.Forms.Button perMed;
        private System.Windows.Forms.GroupBox patGB;
        private System.Windows.Forms.Button lechenieBtn;
        private System.Windows.Forms.Button PatInVidCountBtn;
        private System.Windows.Forms.Button addPatBtn;
        private System.Windows.Forms.GroupBox databaseGB;
        private System.Windows.Forms.Button renameBtn;
        private System.Windows.Forms.Button backupBtn;
        private System.Windows.Forms.Button addTableBtn;
        private System.Windows.Forms.Button delTableBtn;
        private System.Windows.Forms.Label statusLbl;
        private System.Windows.Forms.SaveFileDialog delDlg;
        private System.Windows.Forms.Button JournalBtn;
        private System.Windows.Forms.Button TruncateBtn;
        private System.Windows.Forms.Button tableDataBtn;
    }
}