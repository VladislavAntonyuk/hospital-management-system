﻿namespace HospitalManagementSystem.Reports
{
    partial class PatInVidCrossTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.patInVidCrossTabDGV = new System.Windows.Forms.DataGridView();
            this.saveBtn = new System.Windows.Forms.Button();
            this.saveReportDlg = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.patInVidCrossTabDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // patInVidCrossTabDGV
            // 
            this.patInVidCrossTabDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patInVidCrossTabDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.patInVidCrossTabDGV.Location = new System.Drawing.Point(0, 0);
            this.patInVidCrossTabDGV.Name = "patInVidCrossTabDGV";
            this.patInVidCrossTabDGV.Size = new System.Drawing.Size(854, 262);
            this.patInVidCrossTabDGV.TabIndex = 0;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(779, 239);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 1;
            this.saveBtn.Text = "Зберегти";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // PatInVidCrossTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 262);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.patInVidCrossTabDGV);
            this.Name = "PatInVidCrossTab";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пациенты в отделении";
            this.Load += new System.EventHandler(this.PatInVidCrossTab_Load);
            this.Resize += new System.EventHandler(this.PatInVidCrossTab_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.patInVidCrossTabDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView patInVidCrossTabDGV;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.SaveFileDialog saveReportDlg;
    }
}