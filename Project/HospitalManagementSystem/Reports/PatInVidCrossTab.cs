﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.Reports
{
    public partial class PatInVidCrossTab : Form
    {
        public PatInVidCrossTab()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        private void PatInVidCrossTab_Load(object sender, EventArgs e)
        {
            saveBtn.Location = new Point(this.Size - saveBtn.Size - new Size(15, saveBtn.Height + 15));
            conn = new SqlConnection(GlobalVariables.ConnectionString);
            conn.Open();
            SqlCommand sqlCrossTab = new SqlCommand(@"SELECT *
FROM (
    SELECT Пациент_Лечение.Код_пациента AS pat, Результат_лечения.Название AS result, Отделение.Название
FROM Результат_лечения INNER JOIN (Отделение INNER JOIN (Палата INNER JOIN Пациент_Лечение ON Палата.Код = Пациент_Лечение.Палата) ON Отделение.Код = Палата.Отделение) ON Результат_лечения.Код = Пациент_Лечение.Результат_лечения
GROUP BY Пациент_Лечение.Код_пациента, Результат_лечения.Название, Отделение.Название

) as s
            pivot 
            (
                Count(pat)
                for result in (Выздоровление,[Выписан без изменений],[Выписан с улучшением],[Выписан с ухудшением],[Переведен в другую больницу или отделение],[Продолжает лечение],Умер)
           ) p", conn);

            using (var adapter = new SqlDataAdapter(sqlCrossTab))
            {

                var myTable = new DataTable();
                adapter.Fill(myTable);
                patInVidCrossTabDGV.DataSource = myTable;

            }

            for (int i = 1; i < patInVidCrossTabDGV.ColumnCount; i++)
            {
                int sum = 0;
                for (int j = 0; j < patInVidCrossTabDGV.ColumnCount - 1; j++)
                       sum += (int)patInVidCrossTabDGV.Rows[j].Cells[i].Value;
                patInVidCrossTabDGV.Rows[patInVidCrossTabDGV.RowCount - 1].Cells[i].Value = sum.ToString();
                
            }
            patInVidCrossTabDGV.Rows[patInVidCrossTabDGV.RowCount - 1].Cells[0].Value = "Общее количество пациентов";


            GlobalVariables.Journal(conn, "Open Patients CrossTab", "PatInVidCrossTab");
        }



        private void saveBtn_Click(object sender, EventArgs e)
        {
            saveReportDlg.Filter = "txt|*.txt";
            saveReportDlg.FileName= "Пациенты в отделении.txt";
            saveReportDlg.InitialDirectory = Environment.CurrentDirectory;
            if (saveReportDlg.ShowDialog() == DialogResult.OK)
            {
                Patient pt = new Patient();
                pt.saveLik(patInVidCrossTabDGV, saveReportDlg.FileName);
                GlobalVariables.Journal(conn, "Saved Patients CrossTab", "PatInVidCrossTab");
            }
        }

        private void PatInVidCrossTab_Resize(object sender, EventArgs e)
        {
            saveBtn.Location = new Point(this.Size - saveBtn.Size - new Size(15, saveBtn.Height + 15));
        }
    }
}

