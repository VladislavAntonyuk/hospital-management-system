﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.Reports
{
    public partial class docYearReportbyPatientForm : Form
    {
        public docYearReportbyPatientForm()
        {
            InitializeComponent();
        }
        
        private void docYearReportbyPatient_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'Report.ОтчетВрачаПоПациентамЗаГод' table. You can move, or remove it, as needed.
             SqlConnection con= new SqlConnection(GlobalVariables.ConnectionString);
            con.Open();
            var productTableAdapter = new Reports.ReportTableAdapters.ОтчетВрачаПоПациентамЗаГодTableAdapter();

            productTableAdapter.Connection = con;
            productTableAdapter.ClearBeforeFill = true;
            //  Report.ОтчетВрачаПоПациентамЗаГод.ConnectionString = GlobalVariables.ConnectionString;
            productTableAdapter.Fill(this.Report.ОтчетВрачаПоПациентамЗаГод, GlobalVariables.Login);

            this.reportViewer1.RefreshReport();
            GlobalVariables.Journal(con, "Open doc Report by patients", "docYearReportbyPatientForm");
        }
        
    }
}
