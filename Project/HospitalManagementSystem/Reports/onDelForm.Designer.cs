﻿namespace HospitalManagementSystem.Reports
{
    partial class onDelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.На_увольнениеBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Report = new HospitalManagementSystem.Reports.Report();
            this.На_увольнениеTableAdapter = new HospitalManagementSystem.Reports.ReportTableAdapters.На_увольнениеTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.На_увольнениеBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Report)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.На_увольнениеBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "HospitalManagementSystem.Reports.onDelDocReport.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(552, 403);
            this.reportViewer1.TabIndex = 0;
            // 
            // На_увольнениеBindingSource
            // 
            this.На_увольнениеBindingSource.DataMember = "На_увольнение";
            this.На_увольнениеBindingSource.DataSource = this.Report;
            // 
            // Report
            // 
            this.Report.DataSetName = "Report";
            this.Report.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // На_увольнениеTableAdapter
            // 
            this.На_увольнениеTableAdapter.ClearBeforeFill = true;
            // 
            // onDelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 403);
            this.Controls.Add(this.reportViewer1);
            this.Name = "onDelForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Кандидати на звільнення (стаж>30)";
            this.Load += new System.EventHandler(this.onDelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.На_увольнениеBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Report)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource На_увольнениеBindingSource;
        private Report Report;
        private ReportTableAdapters.На_увольнениеTableAdapter На_увольнениеTableAdapter;
    }
}