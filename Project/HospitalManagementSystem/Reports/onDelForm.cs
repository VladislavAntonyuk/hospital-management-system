﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HospitalManagementSystem.Reports
{
    public partial class onDelForm : Form
    {
        public onDelForm()
        {
            InitializeComponent();
        }

        private void onDelForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'Report.На_увольнение' table. You can move, or remove it, as needed.
            SqlConnection con = new SqlConnection(GlobalVariables.ConnectionString);
            con.Open();
            var productTableAdapter = new Reports.ReportTableAdapters.На_увольнениеTableAdapter();

            productTableAdapter.Connection = con;
            productTableAdapter.ClearBeforeFill = true;
            //  Report.ОтчетВрачаПоПациентамЗаГод.ConnectionString = GlobalVariables.ConnectionString;
             productTableAdapter.Fill(this.Report.На_увольнение, GlobalVariables.Dolgnost);

            this.reportViewer1.RefreshReport();

            GlobalVariables.Journal(con, "Open Report on Del" + GlobalVariables.Dolgnost, "onDelForm");
        }
    }
}
