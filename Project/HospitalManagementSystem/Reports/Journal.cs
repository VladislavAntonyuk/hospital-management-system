﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HospitalManagementSystem.Reports
{
    public partial class Journal : Form
    {
        public Journal()
        {
            InitializeComponent();
        }

        private void Journal_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(GlobalVariables.ConnectionString);
            con.Open();
            GlobalVariables.Journal(con, "Open Journal", "Journal");
            SqlCommand select = new SqlCommand(@"Select * from Журнал ORDER BY Дата DESC", con);
            SqlDataAdapter adap = new SqlDataAdapter(select);
            //SqlCommandBuilder build = new SqlCommandBuilder(adap);
            DataSet ds = new DataSet();
            adap.Fill(ds,"Journal");
            dataResultDGV.DataSource = ds.Tables[0];
        }
    }
}
