USE [HospitalManagementSystem]
GO
/****** Object:  StoredProcedure [dbo].[CreatePatientPass]    Script Date: 5/14/2016 7:24:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Vladislav Antonyuk>
-- Create date: <5/12/2016>
-- Description:	<Create password for Patient>
-- =============================================
ALTER PROCEDURE [dbo].[CreatePatientPass]
	-- Add the parameters for the stored procedure here
	

@cod varchar(50),
@fio varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Insert statements for procedure here
DECLARE	 @Pass varchar(50) 
DECLARE @BD datetime2(0) 

SELECT @BD = Дата_рождения
FROM Пациент sp
WHERE Код = @cod

Declare @spaceindex int = (SELECT charindex(' ',@fio,1))
Declare @doc varchar(50) = (SELECT (substring(@fio,1,@spaceindex-1)))

SET @Pass = (@doc + CONVERT(varchar(10),(year(@BD)),108))

UPDATE Пациент SET Password = @Pass WHERE (Код) = @cod
END

