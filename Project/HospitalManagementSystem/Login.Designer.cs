﻿namespace HospitalManagementSystem
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.nameLbl = new System.Windows.Forms.Label();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.loginBtn = new System.Windows.Forms.Button();
            this.passTxt = new System.Windows.Forms.TextBox();
            this.passLbl = new System.Windows.Forms.Label();
            this.typeCmb = new System.Windows.Forms.ComboBox();
            this.typeLbl = new System.Windows.Forms.Label();
            this.conStrValue = new System.Windows.Forms.TextBox();
            this.constrLbl = new System.Windows.Forms.Label();
            this.helpTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(29, 32);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(38, 13);
            this.nameLbl.TabIndex = 0;
            this.nameLbl.Text = "Логин";
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(76, 29);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(170, 20);
            this.nameTxt.TabIndex = 2;
            this.helpTip.SetToolTip(this.nameTxt, "Введіть Ваше ПІБ");
            // 
            // loginBtn
            // 
            this.loginBtn.Location = new System.Drawing.Point(47, 109);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(162, 23);
            this.loginBtn.TabIndex = 5;
            this.loginBtn.Text = "Вхід";
            this.helpTip.SetToolTip(this.loginBtn, "Натисніть щоб увійти");
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
            // 
            // passTxt
            // 
            this.passTxt.Location = new System.Drawing.Point(76, 55);
            this.passTxt.Name = "passTxt";
            this.passTxt.PasswordChar = '*';
            this.passTxt.Size = new System.Drawing.Size(170, 20);
            this.passTxt.TabIndex = 3;
            this.helpTip.SetToolTip(this.passTxt, "Введіть Ваш пароль");
            // 
            // passLbl
            // 
            this.passLbl.AutoSize = true;
            this.passLbl.Location = new System.Drawing.Point(25, 58);
            this.passLbl.Name = "passLbl";
            this.passLbl.Size = new System.Drawing.Size(45, 13);
            this.passLbl.TabIndex = 3;
            this.passLbl.Text = "Пароль";
            // 
            // typeCmb
            // 
            this.typeCmb.FormattingEnabled = true;
            this.typeCmb.Items.AddRange(new object[] {
            "Доктор",
            "Медсестра",
            "Пациент"});
            this.typeCmb.Location = new System.Drawing.Point(76, 82);
            this.typeCmb.Name = "typeCmb";
            this.typeCmb.Size = new System.Drawing.Size(170, 21);
            this.typeCmb.TabIndex = 4;
            this.helpTip.SetToolTip(this.typeCmb, "Оберіть Ваше звання");
            // 
            // typeLbl
            // 
            this.typeLbl.AutoSize = true;
            this.typeLbl.Location = new System.Drawing.Point(44, 85);
            this.typeLbl.Name = "typeLbl";
            this.typeLbl.Size = new System.Drawing.Size(26, 13);
            this.typeLbl.TabIndex = 6;
            this.typeLbl.Text = "Тип";
            // 
            // conStrValue
            // 
            this.conStrValue.Location = new System.Drawing.Point(76, 3);
            this.conStrValue.Name = "conStrValue";
            this.conStrValue.Size = new System.Drawing.Size(170, 20);
            this.conStrValue.TabIndex = 1;
            this.helpTip.SetToolTip(this.conStrValue, "Введіть ім\'я сервера");
            this.conStrValue.TextChanged += new System.EventHandler(this.conStrValue_TextChanged);
            // 
            // constrLbl
            // 
            this.constrLbl.AutoSize = true;
            this.constrLbl.Location = new System.Drawing.Point(3, 6);
            this.constrLbl.Name = "constrLbl";
            this.constrLbl.Size = new System.Drawing.Size(67, 13);
            this.constrLbl.TabIndex = 8;
            this.constrLbl.Text = "Data Source";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 141);
            this.Controls.Add(this.constrLbl);
            this.Controls.Add(this.conStrValue);
            this.Controls.Add(this.typeLbl);
            this.Controls.Add(this.typeCmb);
            this.Controls.Add(this.passTxt);
            this.Controls.Add(this.passLbl);
            this.Controls.Add(this.loginBtn);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.nameLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизація";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Login_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.Button loginBtn;
        private System.Windows.Forms.TextBox passTxt;
        private System.Windows.Forms.Label passLbl;
        private System.Windows.Forms.ComboBox typeCmb;
        private System.Windows.Forms.Label typeLbl;
        private System.Windows.Forms.TextBox conStrValue;
        private System.Windows.Forms.Label constrLbl;
        private System.Windows.Forms.ToolTip helpTip;
    }
}

