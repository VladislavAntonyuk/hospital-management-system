﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using HospitalManagementSystem.Properties;
using System.IO;

namespace HospitalManagementSystem
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        void loginToProfile()
        {
            loginBtn.Text = "Йде авторизація";
            GlobalVariables.ConnectionString = conStrValue.Text;
            conn = new SqlConnection(GlobalVariables.ConnectionString);
            try
            {
                conn.Open();

                GlobalVariables.Login = nameTxt.Text;

                if (nameTxt.Text != "admin")
                {
                    using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from " + typeCmb.Text + " where ФИО like @username AND Password like @password", conn))
                    {
                        sqlCommand.Parameters.AddWithValue("@username", nameTxt.Text);
                        sqlCommand.Parameters.AddWithValue("@password", passTxt.Text);
                        int userCount = (int)sqlCommand.ExecuteScalar();
                        if (userCount > 0)
                        {
                            GlobalVariables.Journal(conn, "succesfully login", "Login");
                            switch (typeCmb.Text)
                            {
                                case "Доктор":
                                    Doctor dct = new Doctor();
                                    dct.Show();
                                    this.Hide();
                                    break;
                                case "Медсестра":
                                    Nurse med = new Nurse();
                                    med.Show();
                                    this.Hide();
                                    break;
                                case "Пациент":
                                    Patient pat = new Patient();
                                    pat.Show();
                                    this.Hide();
                                    break;

                            }
                        }
                        else
                        {
                            GlobalVariables.Journal(conn, "incorrect username or password", "Login");
                            MessageBox.Show("Перевiрте логiн та пароль", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            loginBtn.Text = "Вхід";
                        }
                    }
                }
                else if (passTxt.Text == "admin")
                {
                    GlobalVariables.Journal(conn, "succesfully login", "Login");
                    Admin adm = new Admin();
                    adm.Show();
                    this.Hide();
                }
                else
                {
                    GlobalVariables.Journal(conn, "incorrect username or password", "Doctor");
                    MessageBox.Show("Перевiрте логiн та пароль", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    loginBtn.Text = "Вхід";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Перевірте підключення" + Environment.NewLine + ex.Message.ToString());
                loginBtn.Text = "Вхід";
            }
        }
        SqlConnection conn;

        private void loginBtn_Click(object sender, EventArgs e)
        {
            loginToProfile();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            conStrValue.Text = Settings.Default.DataSource;// GlobalVariables.ConnectionString.Substring(GlobalVariables.ConnectionString.IndexOf("=") + 1, GlobalVariables.ConnectionString.IndexOf(";") - GlobalVariables.ConnectionString.IndexOf("=") - 1);
            nameTxt.Text = GlobalVariables.Login;
            typeCmb.SelectedIndex = 0;
            if (!new DirectoryInfo(Environment.CurrentDirectory + @"\Report").Exists)
            {
                Directory.CreateDirectory(Environment.CurrentDirectory + @"\Report");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Login_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    Help hlp = new Help(); hlp.ShowDialog();
                    break;
                case Keys.Enter:
                    loginToProfile();
                    break;
            }
        }

        private void conStrValue_TextChanged(object sender, EventArgs e)
        {
            Settings.Default.DataSource = conStrValue.Text;
            Settings.Default.Save();
        }
    }
}