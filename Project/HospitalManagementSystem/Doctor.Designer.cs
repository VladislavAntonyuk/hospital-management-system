﻿namespace HospitalManagementSystem
{
    partial class Doctor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.welcomeLbl = new System.Windows.Forms.Label();
            this.InfoGrB = new System.Windows.Forms.GroupBox();
            this.otdelValue = new System.Windows.Forms.Label();
            this.otdelLbl = new System.Windows.Forms.Label();
            this.stasValue = new System.Windows.Forms.Label();
            this.stashLbl = new System.Windows.Forms.Label();
            this.adrValue = new System.Windows.Forms.Label();
            this.telValue = new System.Windows.Forms.Label();
            this.kvalValue = new System.Windows.Forms.Label();
            this.specValue = new System.Windows.Forms.Label();
            this.priemValue = new System.Windows.Forms.Label();
            this.btdValue = new System.Windows.Forms.Label();
            this.adrLbl = new System.Windows.Forms.Label();
            this.telLbl = new System.Windows.Forms.Label();
            this.kvalLbl = new System.Windows.Forms.Label();
            this.specLbl = new System.Windows.Forms.Label();
            this.priemLbl = new System.Windows.Forms.Label();
            this.drLbl = new System.Windows.Forms.Label();
            this.curpatientGB = new System.Windows.Forms.GroupBox();
            this.curpatientDGV = new System.Windows.Forms.DataGridView();
            this.Info = new System.Windows.Forms.GroupBox();
            this.koikaWorkValue = new System.Windows.Forms.Label();
            this.koikaWorkLbl = new System.Windows.Forms.Label();
            this.nagrValue = new System.Windows.Forms.Label();
            this.nagrLbl = new System.Windows.Forms.Label();
            this.numPatThisYearValue = new System.Windows.Forms.Label();
            this.numpatThisYearLbl = new System.Windows.Forms.Label();
            this.koikaDenValue = new System.Windows.Forms.Label();
            this.koikaDenLbl = new System.Windows.Forms.Label();
            this.LogoutBtn = new System.Windows.Forms.Button();
            this.allPatGB = new System.Windows.Forms.GroupBox();
            this.allPatientDGV = new System.Windows.Forms.DataGridView();
            this.patGB = new System.Windows.Forms.GroupBox();
            this.zvitDocBtn = new System.Windows.Forms.Button();
            this.SaveAllBtn = new System.Windows.Forms.Button();
            this.saveActiveBtn = new System.Windows.Forms.Button();
            this.searchPatBtn = new System.Windows.Forms.Button();
            this.actPatSearchBtn = new System.Windows.Forms.Button();
            this.resLec = new System.Windows.Forms.GroupBox();
            this.prodValue = new System.Windows.Forms.Label();
            this.pomerValue = new System.Windows.Forms.Label();
            this.perevodValue = new System.Windows.Forms.Label();
            this.pogirValue = new System.Windows.Forms.Label();
            this.bezzminValue = new System.Windows.Forms.Label();
            this.pokrValue = new System.Windows.Forms.Label();
            this.vizdorovValue = new System.Windows.Forms.Label();
            this.prodLbl = new System.Windows.Forms.Label();
            this.pomerLbl = new System.Windows.Forms.Label();
            this.perevodLbl = new System.Windows.Forms.Label();
            this.pogirLbl = new System.Windows.Forms.Label();
            this.bezzminLbl = new System.Windows.Forms.Label();
            this.pokrLbl = new System.Windows.Forms.Label();
            this.vizdorovlbl = new System.Windows.Forms.Label();
            this.saveActiveAsBtn = new System.Windows.Forms.Button();
            this.saveAllAsBtn = new System.Windows.Forms.Button();
            this.saveAsDialog = new System.Windows.Forms.SaveFileDialog();
            this.InfoGrB.SuspendLayout();
            this.curpatientGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.curpatientDGV)).BeginInit();
            this.Info.SuspendLayout();
            this.allPatGB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allPatientDGV)).BeginInit();
            this.patGB.SuspendLayout();
            this.resLec.SuspendLayout();
            this.SuspendLayout();
            // 
            // welcomeLbl
            // 
            this.welcomeLbl.AutoSize = true;
            this.welcomeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.welcomeLbl.Location = new System.Drawing.Point(12, 9);
            this.welcomeLbl.Name = "welcomeLbl";
            this.welcomeLbl.Size = new System.Drawing.Size(259, 17);
            this.welcomeLbl.TabIndex = 0;
            this.welcomeLbl.Text = "Добро пожаловать, @doctorname";
            // 
            // InfoGrB
            // 
            this.InfoGrB.Controls.Add(this.otdelValue);
            this.InfoGrB.Controls.Add(this.otdelLbl);
            this.InfoGrB.Controls.Add(this.stasValue);
            this.InfoGrB.Controls.Add(this.stashLbl);
            this.InfoGrB.Controls.Add(this.adrValue);
            this.InfoGrB.Controls.Add(this.telValue);
            this.InfoGrB.Controls.Add(this.kvalValue);
            this.InfoGrB.Controls.Add(this.specValue);
            this.InfoGrB.Controls.Add(this.priemValue);
            this.InfoGrB.Controls.Add(this.btdValue);
            this.InfoGrB.Controls.Add(this.adrLbl);
            this.InfoGrB.Controls.Add(this.telLbl);
            this.InfoGrB.Controls.Add(this.kvalLbl);
            this.InfoGrB.Controls.Add(this.specLbl);
            this.InfoGrB.Controls.Add(this.priemLbl);
            this.InfoGrB.Controls.Add(this.drLbl);
            this.InfoGrB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InfoGrB.Location = new System.Drawing.Point(12, 39);
            this.InfoGrB.Name = "InfoGrB";
            this.InfoGrB.Size = new System.Drawing.Size(427, 234);
            this.InfoGrB.TabIndex = 1;
            this.InfoGrB.TabStop = false;
            this.InfoGrB.Text = "Личная информация";
            // 
            // otdelValue
            // 
            this.otdelValue.AutoSize = true;
            this.otdelValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.otdelValue.Location = new System.Drawing.Point(119, 207);
            this.otdelValue.Name = "otdelValue";
            this.otdelValue.Size = new System.Drawing.Size(62, 13);
            this.otdelValue.TabIndex = 15;
            this.otdelValue.Text = "Отделение";
            // 
            // otdelLbl
            // 
            this.otdelLbl.AutoSize = true;
            this.otdelLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.otdelLbl.Location = new System.Drawing.Point(6, 207);
            this.otdelLbl.Name = "otdelLbl";
            this.otdelLbl.Size = new System.Drawing.Size(62, 13);
            this.otdelLbl.TabIndex = 14;
            this.otdelLbl.Text = "Отделение";
            // 
            // stasValue
            // 
            this.stasValue.AutoSize = true;
            this.stasValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stasValue.Location = new System.Drawing.Point(119, 183);
            this.stasValue.Name = "stasValue";
            this.stasValue.Size = new System.Drawing.Size(33, 13);
            this.stasValue.TabIndex = 13;
            this.stasValue.Text = "Стаж";
            // 
            // stashLbl
            // 
            this.stashLbl.AutoSize = true;
            this.stashLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stashLbl.Location = new System.Drawing.Point(6, 183);
            this.stashLbl.Name = "stashLbl";
            this.stashLbl.Size = new System.Drawing.Size(33, 13);
            this.stashLbl.TabIndex = 12;
            this.stashLbl.Text = "Стаж";
            // 
            // adrValue
            // 
            this.adrValue.AutoSize = true;
            this.adrValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrValue.Location = new System.Drawing.Point(119, 157);
            this.adrValue.Name = "adrValue";
            this.adrValue.Size = new System.Drawing.Size(22, 13);
            this.adrValue.TabIndex = 11;
            this.adrValue.Text = "adr";
            // 
            // telValue
            // 
            this.telValue.AutoSize = true;
            this.telValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telValue.Location = new System.Drawing.Point(119, 132);
            this.telValue.Name = "telValue";
            this.telValue.Size = new System.Drawing.Size(86, 13);
            this.telValue.TabIndex = 10;
            this.telValue.Text = "Дата рождения";
            // 
            // kvalValue
            // 
            this.kvalValue.AutoSize = true;
            this.kvalValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalValue.Location = new System.Drawing.Point(119, 106);
            this.kvalValue.Name = "kvalValue";
            this.kvalValue.Size = new System.Drawing.Size(86, 13);
            this.kvalValue.TabIndex = 9;
            this.kvalValue.Text = "Дата рождения";
            // 
            // specValue
            // 
            this.specValue.AutoSize = true;
            this.specValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specValue.Location = new System.Drawing.Point(119, 76);
            this.specValue.Name = "specValue";
            this.specValue.Size = new System.Drawing.Size(86, 13);
            this.specValue.TabIndex = 8;
            this.specValue.Text = "Дата рождения";
            // 
            // priemValue
            // 
            this.priemValue.AutoSize = true;
            this.priemValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priemValue.Location = new System.Drawing.Point(119, 47);
            this.priemValue.Name = "priemValue";
            this.priemValue.Size = new System.Drawing.Size(86, 13);
            this.priemValue.TabIndex = 7;
            this.priemValue.Text = "Дата рождения";
            // 
            // btdValue
            // 
            this.btdValue.AutoSize = true;
            this.btdValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btdValue.Location = new System.Drawing.Point(119, 20);
            this.btdValue.Name = "btdValue";
            this.btdValue.Size = new System.Drawing.Size(86, 13);
            this.btdValue.TabIndex = 6;
            this.btdValue.Text = "Дата рождения";
            // 
            // adrLbl
            // 
            this.adrLbl.AutoSize = true;
            this.adrLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.adrLbl.Location = new System.Drawing.Point(6, 157);
            this.adrLbl.Name = "adrLbl";
            this.adrLbl.Size = new System.Drawing.Size(38, 13);
            this.adrLbl.TabIndex = 5;
            this.adrLbl.Text = "Адрес";
            // 
            // telLbl
            // 
            this.telLbl.AutoSize = true;
            this.telLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.telLbl.Location = new System.Drawing.Point(6, 132);
            this.telLbl.Name = "telLbl";
            this.telLbl.Size = new System.Drawing.Size(52, 13);
            this.telLbl.TabIndex = 4;
            this.telLbl.Text = "Телефон";
            // 
            // kvalLbl
            // 
            this.kvalLbl.AutoSize = true;
            this.kvalLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kvalLbl.Location = new System.Drawing.Point(6, 106);
            this.kvalLbl.Name = "kvalLbl";
            this.kvalLbl.Size = new System.Drawing.Size(82, 13);
            this.kvalLbl.TabIndex = 3;
            this.kvalLbl.Text = "Квалификация";
            // 
            // specLbl
            // 
            this.specLbl.AutoSize = true;
            this.specLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specLbl.Location = new System.Drawing.Point(6, 76);
            this.specLbl.Name = "specLbl";
            this.specLbl.Size = new System.Drawing.Size(86, 13);
            this.specLbl.TabIndex = 2;
            this.specLbl.Text = "Специализация";
            // 
            // priemLbl
            // 
            this.priemLbl.AutoSize = true;
            this.priemLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.priemLbl.Location = new System.Drawing.Point(6, 47);
            this.priemLbl.Name = "priemLbl";
            this.priemLbl.Size = new System.Drawing.Size(74, 13);
            this.priemLbl.TabIndex = 1;
            this.priemLbl.Text = "Дата приема";
            // 
            // drLbl
            // 
            this.drLbl.AutoSize = true;
            this.drLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.drLbl.Location = new System.Drawing.Point(6, 20);
            this.drLbl.Name = "drLbl";
            this.drLbl.Size = new System.Drawing.Size(98, 13);
            this.drLbl.TabIndex = 0;
            this.drLbl.Text = "Дата народження";
            // 
            // curpatientGB
            // 
            this.curpatientGB.Controls.Add(this.curpatientDGV);
            this.curpatientGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.curpatientGB.Location = new System.Drawing.Point(6, 20);
            this.curpatientGB.Name = "curpatientGB";
            this.curpatientGB.Size = new System.Drawing.Size(367, 235);
            this.curpatientGB.TabIndex = 12;
            this.curpatientGB.TabStop = false;
            this.curpatientGB.Text = "Активні пацієнти";
            // 
            // curpatientDGV
            // 
            this.curpatientDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.curpatientDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.curpatientDGV.Location = new System.Drawing.Point(3, 16);
            this.curpatientDGV.Name = "curpatientDGV";
            this.curpatientDGV.Size = new System.Drawing.Size(361, 216);
            this.curpatientDGV.TabIndex = 0;
            // 
            // Info
            // 
            this.Info.Controls.Add(this.koikaWorkValue);
            this.Info.Controls.Add(this.koikaWorkLbl);
            this.Info.Controls.Add(this.nagrValue);
            this.Info.Controls.Add(this.nagrLbl);
            this.Info.Controls.Add(this.numPatThisYearValue);
            this.Info.Controls.Add(this.numpatThisYearLbl);
            this.Info.Controls.Add(this.koikaDenValue);
            this.Info.Controls.Add(this.koikaDenLbl);
            this.Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Info.Location = new System.Drawing.Point(12, 279);
            this.Info.Name = "Info";
            this.Info.Size = new System.Drawing.Size(427, 126);
            this.Info.TabIndex = 12;
            this.Info.TabStop = false;
            this.Info.Text = "Интерисующие дані";
            // 
            // koikaWorkValue
            // 
            this.koikaWorkValue.AutoSize = true;
            this.koikaWorkValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.koikaWorkValue.Location = new System.Drawing.Point(179, 103);
            this.koikaWorkValue.Name = "koikaWorkValue";
            this.koikaWorkValue.Size = new System.Drawing.Size(167, 13);
            this.koikaWorkValue.TabIndex = 12;
            this.koikaWorkValue.Text = "Кількість пацієнтів в цьому році";
            // 
            // koikaWorkLbl
            // 
            this.koikaWorkLbl.AutoSize = true;
            this.koikaWorkLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.koikaWorkLbl.Location = new System.Drawing.Point(6, 103);
            this.koikaWorkLbl.Name = "koikaWorkLbl";
            this.koikaWorkLbl.Size = new System.Drawing.Size(152, 13);
            this.koikaWorkLbl.TabIndex = 11;
            this.koikaWorkLbl.Text = "Робота койки в цьому місяці";
            // 
            // nagrValue
            // 
            this.nagrValue.AutoSize = true;
            this.nagrValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nagrValue.Location = new System.Drawing.Point(179, 75);
            this.nagrValue.Name = "nagrValue";
            this.nagrValue.Size = new System.Drawing.Size(167, 13);
            this.nagrValue.TabIndex = 10;
            this.nagrValue.Text = "Кількість пацієнтів в цьому році";
            // 
            // nagrLbl
            // 
            this.nagrLbl.AutoSize = true;
            this.nagrLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nagrLbl.Location = new System.Drawing.Point(6, 75);
            this.nagrLbl.Name = "nagrLbl";
            this.nagrLbl.Size = new System.Drawing.Size(154, 13);
            this.nagrLbl.TabIndex = 9;
            this.nagrLbl.Text = "Навантаженння в цьому році";
            // 
            // numPatThisYearValue
            // 
            this.numPatThisYearValue.AutoSize = true;
            this.numPatThisYearValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numPatThisYearValue.Location = new System.Drawing.Point(179, 47);
            this.numPatThisYearValue.Name = "numPatThisYearValue";
            this.numPatThisYearValue.Size = new System.Drawing.Size(86, 13);
            this.numPatThisYearValue.TabIndex = 8;
            this.numPatThisYearValue.Text = "Дата рождения";
            // 
            // numpatThisYearLbl
            // 
            this.numpatThisYearLbl.AutoSize = true;
            this.numpatThisYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numpatThisYearLbl.Location = new System.Drawing.Point(6, 47);
            this.numpatThisYearLbl.Name = "numpatThisYearLbl";
            this.numpatThisYearLbl.Size = new System.Drawing.Size(177, 13);
            this.numpatThisYearLbl.TabIndex = 7;
            this.numpatThisYearLbl.Text = "Кількість пацієнтів в цьому місяці";
            // 
            // koikaDenValue
            // 
            this.koikaDenValue.AutoSize = true;
            this.koikaDenValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.koikaDenValue.Location = new System.Drawing.Point(179, 20);
            this.koikaDenValue.Name = "koikaDenValue";
            this.koikaDenValue.Size = new System.Drawing.Size(86, 13);
            this.koikaDenValue.TabIndex = 6;
            this.koikaDenValue.Text = "Дата рождения";
            // 
            // koikaDenLbl
            // 
            this.koikaDenLbl.AutoSize = true;
            this.koikaDenLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.koikaDenLbl.Location = new System.Drawing.Point(6, 20);
            this.koikaDenLbl.Name = "koikaDenLbl";
            this.koikaDenLbl.Size = new System.Drawing.Size(164, 13);
            this.koikaDenLbl.TabIndex = 0;
            this.koikaDenLbl.Text = "Середній койка-день за місяць";
            // 
            // LogoutBtn
            // 
            this.LogoutBtn.Location = new System.Drawing.Point(737, 9);
            this.LogoutBtn.Name = "LogoutBtn";
            this.LogoutBtn.Size = new System.Drawing.Size(75, 23);
            this.LogoutBtn.TabIndex = 13;
            this.LogoutBtn.Text = "Вихід";
            this.LogoutBtn.UseVisualStyleBackColor = true;
            this.LogoutBtn.Click += new System.EventHandler(this.LogoutBtn_Click);
            // 
            // allPatGB
            // 
            this.allPatGB.Controls.Add(this.allPatientDGV);
            this.allPatGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.allPatGB.Location = new System.Drawing.Point(6, 270);
            this.allPatGB.Name = "allPatGB";
            this.allPatGB.Size = new System.Drawing.Size(367, 287);
            this.allPatGB.TabIndex = 13;
            this.allPatGB.TabStop = false;
            this.allPatGB.Text = "Архів пацієнтів";
            // 
            // allPatientDGV
            // 
            this.allPatientDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.allPatientDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.allPatientDGV.Location = new System.Drawing.Point(3, 16);
            this.allPatientDGV.Name = "allPatientDGV";
            this.allPatientDGV.Size = new System.Drawing.Size(361, 268);
            this.allPatientDGV.TabIndex = 0;
            // 
            // patGB
            // 
            this.patGB.Controls.Add(this.zvitDocBtn);
            this.patGB.Controls.Add(this.curpatientGB);
            this.patGB.Controls.Add(this.allPatGB);
            this.patGB.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.patGB.Location = new System.Drawing.Point(445, 39);
            this.patGB.Name = "patGB";
            this.patGB.Size = new System.Drawing.Size(374, 563);
            this.patGB.TabIndex = 14;
            this.patGB.TabStop = false;
            this.patGB.Text = "Пацієнти";
            // 
            // zvitDocBtn
            // 
            this.zvitDocBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zvitDocBtn.Location = new System.Drawing.Point(111, 262);
            this.zvitDocBtn.Name = "zvitDocBtn";
            this.zvitDocBtn.Size = new System.Drawing.Size(67, 23);
            this.zvitDocBtn.TabIndex = 18;
            this.zvitDocBtn.Text = "Звіт за рік";
            this.zvitDocBtn.UseVisualStyleBackColor = true;
            this.zvitDocBtn.Click += new System.EventHandler(this.zvitDocBtn_Click);
            // 
            // SaveAllBtn
            // 
            this.SaveAllBtn.Location = new System.Drawing.Point(674, 301);
            this.SaveAllBtn.Name = "SaveAllBtn";
            this.SaveAllBtn.Size = new System.Drawing.Size(64, 23);
            this.SaveAllBtn.TabIndex = 16;
            this.SaveAllBtn.Text = "Зберегти";
            this.SaveAllBtn.UseVisualStyleBackColor = true;
            this.SaveAllBtn.Click += new System.EventHandler(this.SaveAllBtn_Click);
            // 
            // saveActiveBtn
            // 
            this.saveActiveBtn.Location = new System.Drawing.Point(674, 51);
            this.saveActiveBtn.Name = "saveActiveBtn";
            this.saveActiveBtn.Size = new System.Drawing.Size(64, 23);
            this.saveActiveBtn.TabIndex = 15;
            this.saveActiveBtn.Text = "Зберегти";
            this.saveActiveBtn.UseVisualStyleBackColor = true;
            this.saveActiveBtn.Click += new System.EventHandler(this.saveActiveBtn_Click);
            // 
            // searchPatBtn
            // 
            this.searchPatBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPatBtn.Location = new System.Drawing.Point(622, 301);
            this.searchPatBtn.Name = "searchPatBtn";
            this.searchPatBtn.Size = new System.Drawing.Size(53, 23);
            this.searchPatBtn.TabIndex = 17;
            this.searchPatBtn.Text = "Пошук";
            this.searchPatBtn.UseVisualStyleBackColor = true;
            this.searchPatBtn.Click += new System.EventHandler(this.searchPatBtn_Click);
            // 
            // actPatSearchBtn
            // 
            this.actPatSearchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actPatSearchBtn.Location = new System.Drawing.Point(622, 51);
            this.actPatSearchBtn.Name = "actPatSearchBtn";
            this.actPatSearchBtn.Size = new System.Drawing.Size(53, 23);
            this.actPatSearchBtn.TabIndex = 18;
            this.actPatSearchBtn.Text = "Пошук";
            this.actPatSearchBtn.UseVisualStyleBackColor = true;
            this.actPatSearchBtn.Click += new System.EventHandler(this.actPatSearchBtn_Click);
            // 
            // resLec
            // 
            this.resLec.Controls.Add(this.prodValue);
            this.resLec.Controls.Add(this.pomerValue);
            this.resLec.Controls.Add(this.perevodValue);
            this.resLec.Controls.Add(this.pogirValue);
            this.resLec.Controls.Add(this.bezzminValue);
            this.resLec.Controls.Add(this.pokrValue);
            this.resLec.Controls.Add(this.vizdorovValue);
            this.resLec.Controls.Add(this.prodLbl);
            this.resLec.Controls.Add(this.pomerLbl);
            this.resLec.Controls.Add(this.perevodLbl);
            this.resLec.Controls.Add(this.pogirLbl);
            this.resLec.Controls.Add(this.bezzminLbl);
            this.resLec.Controls.Add(this.pokrLbl);
            this.resLec.Controls.Add(this.vizdorovlbl);
            this.resLec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resLec.Location = new System.Drawing.Point(15, 411);
            this.resLec.Name = "resLec";
            this.resLec.Size = new System.Drawing.Size(427, 191);
            this.resLec.TabIndex = 13;
            this.resLec.TabStop = false;
            this.resLec.Text = "Результати лікування";
            // 
            // prodValue
            // 
            this.prodValue.AutoSize = true;
            this.prodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prodValue.Location = new System.Drawing.Point(226, 162);
            this.prodValue.Name = "prodValue";
            this.prodValue.Size = new System.Drawing.Size(116, 13);
            this.prodValue.TabIndex = 26;
            this.prodValue.Text = "Продовжує лікування";
            // 
            // pomerValue
            // 
            this.pomerValue.AutoSize = true;
            this.pomerValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pomerValue.Location = new System.Drawing.Point(226, 140);
            this.pomerValue.Name = "pomerValue";
            this.pomerValue.Size = new System.Drawing.Size(41, 13);
            this.pomerValue.TabIndex = 25;
            this.pomerValue.Text = "Помер";
            // 
            // perevodValue
            // 
            this.perevodValue.AutoSize = true;
            this.perevodValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.perevodValue.Location = new System.Drawing.Point(226, 116);
            this.perevodValue.Name = "perevodValue";
            this.perevodValue.Size = new System.Drawing.Size(46, 13);
            this.perevodValue.TabIndex = 24;
            this.perevodValue.Text = "perevod";
            // 
            // pogirValue
            // 
            this.pogirValue.AutoSize = true;
            this.pogirValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pogirValue.Location = new System.Drawing.Point(226, 94);
            this.pogirValue.Name = "pogirValue";
            this.pogirValue.Size = new System.Drawing.Size(127, 13);
            this.pogirValue.TabIndex = 23;
            this.pogirValue.Text = "Виписан з погіршенням";
            // 
            // bezzminValue
            // 
            this.bezzminValue.AutoSize = true;
            this.bezzminValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bezzminValue.Location = new System.Drawing.Point(226, 72);
            this.bezzminValue.Name = "bezzminValue";
            this.bezzminValue.Size = new System.Drawing.Size(96, 13);
            this.bezzminValue.TabIndex = 22;
            this.bezzminValue.Text = "Виписан без змін";
            // 
            // pokrValue
            // 
            this.pokrValue.AutoSize = true;
            this.pokrValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pokrValue.Location = new System.Drawing.Point(226, 49);
            this.pokrValue.Name = "pokrValue";
            this.pokrValue.Size = new System.Drawing.Size(133, 13);
            this.pokrValue.TabIndex = 21;
            this.pokrValue.Text = "Виписан з покращенням";
            // 
            // vizdorovValue
            // 
            this.vizdorovValue.AutoSize = true;
            this.vizdorovValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vizdorovValue.Location = new System.Drawing.Point(226, 27);
            this.vizdorovValue.Name = "vizdorovValue";
            this.vizdorovValue.Size = new System.Drawing.Size(86, 13);
            this.vizdorovValue.TabIndex = 20;
            this.vizdorovValue.Text = "Виздоровлення";
            // 
            // prodLbl
            // 
            this.prodLbl.AutoSize = true;
            this.prodLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prodLbl.Location = new System.Drawing.Point(6, 162);
            this.prodLbl.Name = "prodLbl";
            this.prodLbl.Size = new System.Drawing.Size(116, 13);
            this.prodLbl.TabIndex = 19;
            this.prodLbl.Text = "Продовжує лікування";
            // 
            // pomerLbl
            // 
            this.pomerLbl.AutoSize = true;
            this.pomerLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pomerLbl.Location = new System.Drawing.Point(6, 140);
            this.pomerLbl.Name = "pomerLbl";
            this.pomerLbl.Size = new System.Drawing.Size(41, 13);
            this.pomerLbl.TabIndex = 18;
            this.pomerLbl.Text = "Помер";
            // 
            // perevodLbl
            // 
            this.perevodLbl.AutoSize = true;
            this.perevodLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.perevodLbl.Location = new System.Drawing.Point(6, 116);
            this.perevodLbl.Name = "perevodLbl";
            this.perevodLbl.Size = new System.Drawing.Size(220, 13);
            this.perevodLbl.TabIndex = 17;
            this.perevodLbl.Text = "Переведено в другу лікарню чи відділення";
            // 
            // pogirLbl
            // 
            this.pogirLbl.AutoSize = true;
            this.pogirLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pogirLbl.Location = new System.Drawing.Point(6, 94);
            this.pogirLbl.Name = "pogirLbl";
            this.pogirLbl.Size = new System.Drawing.Size(127, 13);
            this.pogirLbl.TabIndex = 16;
            this.pogirLbl.Text = "Виписан з погіршенням";
            // 
            // bezzminLbl
            // 
            this.bezzminLbl.AutoSize = true;
            this.bezzminLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bezzminLbl.Location = new System.Drawing.Point(6, 72);
            this.bezzminLbl.Name = "bezzminLbl";
            this.bezzminLbl.Size = new System.Drawing.Size(96, 13);
            this.bezzminLbl.TabIndex = 15;
            this.bezzminLbl.Text = "Виписан без змін";
            // 
            // pokrLbl
            // 
            this.pokrLbl.AutoSize = true;
            this.pokrLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pokrLbl.Location = new System.Drawing.Point(6, 49);
            this.pokrLbl.Name = "pokrLbl";
            this.pokrLbl.Size = new System.Drawing.Size(133, 13);
            this.pokrLbl.TabIndex = 14;
            this.pokrLbl.Text = "Виписан з покращенням";
            // 
            // vizdorovlbl
            // 
            this.vizdorovlbl.AutoSize = true;
            this.vizdorovlbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.vizdorovlbl.Location = new System.Drawing.Point(6, 27);
            this.vizdorovlbl.Name = "vizdorovlbl";
            this.vizdorovlbl.Size = new System.Drawing.Size(86, 13);
            this.vizdorovlbl.TabIndex = 13;
            this.vizdorovlbl.Text = "Виздоровлення";
            // 
            // saveActiveAsBtn
            // 
            this.saveActiveAsBtn.Location = new System.Drawing.Point(737, 51);
            this.saveActiveAsBtn.Name = "saveActiveAsBtn";
            this.saveActiveAsBtn.Size = new System.Drawing.Size(78, 23);
            this.saveActiveAsBtn.TabIndex = 19;
            this.saveActiveAsBtn.Text = "Зберегти як";
            this.saveActiveAsBtn.UseVisualStyleBackColor = true;
            this.saveActiveAsBtn.Click += new System.EventHandler(this.saveActiveAsBtn_Click);
            // 
            // saveAllAsBtn
            // 
            this.saveAllAsBtn.Location = new System.Drawing.Point(737, 301);
            this.saveAllAsBtn.Name = "saveAllAsBtn";
            this.saveAllAsBtn.Size = new System.Drawing.Size(78, 23);
            this.saveAllAsBtn.TabIndex = 20;
            this.saveAllAsBtn.Text = "Зберегти як";
            this.saveAllAsBtn.UseVisualStyleBackColor = true;
            this.saveAllAsBtn.Click += new System.EventHandler(this.saveAllAsBtn_Click);
            // 
            // saveAsDialog
            // 
            this.saveAsDialog.DefaultExt = "txt";
            this.saveAsDialog.Filter = "Text files|*.txt";
            this.saveAsDialog.InitialDirectory = "Environment.CurrentDirectory";
            this.saveAsDialog.Title = "Зберегти пацієнтів";
            // 
            // Doctor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 605);
            this.Controls.Add(this.saveActiveAsBtn);
            this.Controls.Add(this.saveAllAsBtn);
            this.Controls.Add(this.resLec);
            this.Controls.Add(this.saveActiveBtn);
            this.Controls.Add(this.actPatSearchBtn);
            this.Controls.Add(this.SaveAllBtn);
            this.Controls.Add(this.searchPatBtn);
            this.Controls.Add(this.patGB);
            this.Controls.Add(this.LogoutBtn);
            this.Controls.Add(this.Info);
            this.Controls.Add(this.InfoGrB);
            this.Controls.Add(this.welcomeLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "Doctor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Доктор";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Doctor_FormClosing);
            this.Load += new System.EventHandler(this.Doctor_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Doctor_KeyDown);
            this.InfoGrB.ResumeLayout(false);
            this.InfoGrB.PerformLayout();
            this.curpatientGB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.curpatientDGV)).EndInit();
            this.Info.ResumeLayout(false);
            this.Info.PerformLayout();
            this.allPatGB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allPatientDGV)).EndInit();
            this.patGB.ResumeLayout(false);
            this.resLec.ResumeLayout(false);
            this.resLec.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label welcomeLbl;
        private System.Windows.Forms.GroupBox InfoGrB;
        private System.Windows.Forms.Label adrValue;
        private System.Windows.Forms.Label telValue;
        private System.Windows.Forms.Label kvalValue;
        private System.Windows.Forms.Label specValue;
        private System.Windows.Forms.Label priemValue;
        private System.Windows.Forms.Label btdValue;
        private System.Windows.Forms.Label adrLbl;
        private System.Windows.Forms.Label telLbl;
        private System.Windows.Forms.Label kvalLbl;
        private System.Windows.Forms.Label specLbl;
        private System.Windows.Forms.Label priemLbl;
        private System.Windows.Forms.Label drLbl;
        private System.Windows.Forms.GroupBox curpatientGB;
        private System.Windows.Forms.DataGridView curpatientDGV;
        private System.Windows.Forms.GroupBox Info;
        private System.Windows.Forms.Label koikaDenValue;
        private System.Windows.Forms.Label koikaDenLbl;
        private System.Windows.Forms.Button LogoutBtn;
        private System.Windows.Forms.Label numPatThisYearValue;
        private System.Windows.Forms.Label numpatThisYearLbl;
        private System.Windows.Forms.Label stasValue;
        private System.Windows.Forms.Label stashLbl;
        private System.Windows.Forms.Label koikaWorkValue;
        private System.Windows.Forms.Label koikaWorkLbl;
        private System.Windows.Forms.Label nagrValue;
        private System.Windows.Forms.Label nagrLbl;
        private System.Windows.Forms.GroupBox allPatGB;
        private System.Windows.Forms.DataGridView allPatientDGV;
        private System.Windows.Forms.GroupBox patGB;
        private System.Windows.Forms.Button SaveAllBtn;
        private System.Windows.Forms.Button saveActiveBtn;
        private System.Windows.Forms.Button searchPatBtn;
        private System.Windows.Forms.Button actPatSearchBtn;
        private System.Windows.Forms.GroupBox resLec;
        private System.Windows.Forms.Label prodValue;
        private System.Windows.Forms.Label pomerValue;
        private System.Windows.Forms.Label perevodValue;
        private System.Windows.Forms.Label pogirValue;
        private System.Windows.Forms.Label bezzminValue;
        private System.Windows.Forms.Label pokrValue;
        private System.Windows.Forms.Label vizdorovValue;
        private System.Windows.Forms.Label prodLbl;
        private System.Windows.Forms.Label pomerLbl;
        private System.Windows.Forms.Label perevodLbl;
        private System.Windows.Forms.Label pogirLbl;
        private System.Windows.Forms.Label bezzminLbl;
        private System.Windows.Forms.Label pokrLbl;
        private System.Windows.Forms.Label vizdorovlbl;
        private System.Windows.Forms.Button zvitDocBtn;
        private System.Windows.Forms.Label otdelValue;
        private System.Windows.Forms.Label otdelLbl;
        private System.Windows.Forms.Button saveActiveAsBtn;
        private System.Windows.Forms.Button saveAllAsBtn;
        private System.Windows.Forms.SaveFileDialog saveAsDialog;
    }
}